// $flisboac 2017-10-22

#define WIN32_LEAN_AND_MEAN  /* required by xmlrpc-c/server_abyss.h */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

// LIB
#include "uvafat_xmlrpcc_.h"
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/server_abyss.h>


/*
 * [[ DEFINIÇÕES ]]
 */


#define XMLRPC_C_DECL(nome) static xmlrpc_value* nome(xmlrpc_env* envP, xmlrpc_value* paramArrayP, void* serverInfo, void* channelInfo)
#define XMLRPC_C_DEFN(nome) xmlrpc_value* nome(xmlrpc_env * const envP, xmlrpc_value * const paramArrayP, void * const serverInfo, void *const channelInfo)


typedef struct uvafat_Instancia_ServidorXmlRpccImpl_ uvafat_Instancia_ServidorXmlRpccImpl_;


struct uvafat_Instancia_ServidorXmlRpccImpl_ {
    uvafat_Instancia_p instancia;
    FILE* arquivoLog;
    FILE* arquivoErr;
    xmlrpc_registry* registry;
    xmlrpc_server_abyss_t * server;
    xmlrpc_server_abyss_sig * oldHandlers;
    xmlrpc_env env;
};


/*
 * [[ FUNÇÕES ]]
 */


// Wrappers da API externa para o XMLRPC-C

XMLRPC_C_DECL(uvafat_criarUsuario__servidorXmlRpccWrapper_);
XMLRPC_C_DECL(uvafat_obterDadosUsuario__servidorXmlRpccWrapper_);

XMLRPC_C_DECL(uvafat_adicionarMeioPagamento__servidorXmlRpccWrapper_);
XMLRPC_C_DECL(uvafat_listarMeiosPagamento__servidorXmlRpccWrapper_);

XMLRPC_C_DECL(uvafat_iniciarTransacao__servidorXmlRpccWrapper_);
XMLRPC_C_DECL(uvafat_listarTransacoes__servidorXmlRpccWrapper_);


// API externa

static void uvafat_finalizar__servidorXmlRpccImpl_(uvafat_Instancia_p S);

static uvafat_IdUsuario uvafat_criarUsuario__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);
static void uvafat_obterDadosUsuario__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);

static uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados);
static size_t uvafat_listarMeiosPagamento__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados);

static uvafat_IdTransacao uvafat_iniciarTransacao__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);
static size_t uvafat_listarTransacoes__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosTransacao* dados, size_t qtdDados);
static void uvafat_finalizarTransacao__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);


// API Interna

static void LOG(uvafat_Instancia_ServidorXmlRpccImpl_* S_, char const* fmt, ...);
static void ERR(uvafat_Instancia_ServidorXmlRpccImpl_* S_, char const* fmt, ...);
static uvafat_Instancia_p uvafat_Instancia_obterImpl_(uvafat_Instancia_p S);
static uvafat_Instancia_ServidorXmlRpccImpl_* uvafat_Instancia_obterWrapper_(uvafat_Instancia_p S);
static uvafat_Bool uvafat_Instancia_criarServidorXmlRpcc_(struct uvafat_Instancia* S, uvafat_Instancia_ServidorXmlRpccImpl_* S_, uvafat_DadosInstanciaServidor* dados, uvafat_IdErro* erro);
static void uvafat_Instancia_finalizarServidorXmlRpcc_(uvafat_Instancia_ServidorXmlRpccImpl_* S_);


/*
 * [[ IMPLEMENTAÇÃO: API PÚBLICA ]]
 */


static const uvafat_Instancia_Api_ uvafat_instancia__xmlRpccApiImpl_ = {
    uvafat_finalizar__servidorXmlRpccImpl_,

    uvafat_criarUsuario__servidorXmlRpccImpl_,
    uvafat_obterDadosUsuario__servidorXmlRpccImpl_,

    uvafat_adicionarMeioPagamento__servidorXmlRpccImpl_,
    uvafat_listarMeiosPagamento__servidorXmlRpccImpl_,

    uvafat_iniciarTransacao__servidorXmlRpccImpl_,
    uvafat_listarTransacoes__servidorXmlRpccImpl_,
    uvafat_finalizarTransacao__servidorXmlRpccImpl_
};

uvafat_Instancia_p uvafat_iniciarServidor(uvafat_DadosInstanciaServidor* dados, uvafat_IdErro* erro) {
    uvafat_IdErro resultado = uvafat_ERR_OK;
    uvafat_Instancia_p I = uvafat_criar(&resultado);
    uvafat_Instancia_p S = NULL;

    if (I) {
        S = malloc(sizeof(struct uvafat_Instancia) + sizeof(uvafat_Instancia_ServidorXmlRpccImpl_));

        if (S) {
            uvafat_Instancia_ServidorXmlRpccImpl_* S_ = (void*) S++;
            S->impl_c = &uvafat_instancia__xmlRpccApiImpl_;
            S->impl_p = S_;
            uvafat_Instancia_limparErro_(S);

            S_->instancia = I;
            S_->arquivoLog = dados->arquivoLog;
            S_->arquivoErr = dados->arquivoErro;
            S_->registry = NULL;
            S_->oldHandlers = NULL;
            S_->server = NULL;

            if (!uvafat_Instancia_criarServidorXmlRpcc_(S, S_, dados, &resultado)) {
                uvafat_finalizar(I);
                free(S);
            }

        } else {
            resultado = uvafat_ERR_MEM;
        }
    }

    if (erro) *erro = resultado;
    return S;
}


// -------


void uvafat_finalizar__servidorXmlRpccImpl_(uvafat_Instancia_p S) {
    if (S) {
        uvafat_Instancia_finalizarServidorXmlRpcc_(uvafat_Instancia_obterWrapper_(S));
        uvafat_finalizar(uvafat_Instancia_obterImpl_(S));
        free(S);
    }
}

uvafat_IdUsuario uvafat_criarUsuario__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    uvafat_IdUsuario idUsuario = uvafat_criarUsuario(I, dados);
    uvafat_Instancia_copiarErro_(S, I);
    return idUsuario;
}

void uvafat_obterDadosUsuario__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    uvafat_obterDadosUsuario(I, dados);
    uvafat_Instancia_copiarErro_(S, I);
}

uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    uvafat_IdMeioPagamento ret = uvafat_adicionarMeioPagamento(I, dados);
    uvafat_Instancia_copiarErro_(S, I);
    return ret;
}

size_t uvafat_listarMeiosPagamento__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    size_t ret = uvafat_listarMeiosPagamento(I, idUsuario, dados, qtdDados);
    uvafat_Instancia_copiarErro_(S, I);
    return ret;
}

uvafat_IdTransacao uvafat_iniciarTransacao__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    uvafat_IdTransacao ret = uvafat_iniciarTransacao(I, dados);
    uvafat_Instancia_copiarErro_(S, I);
    return ret;
}

size_t uvafat_listarTransacoes__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosTransacao* dados, size_t qtdDados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    size_t ret = uvafat_listarTransacoes(I, idUsuario, dados, qtdDados);
    uvafat_Instancia_copiarErro_(S, I);
    return ret;
}

void uvafat_finalizarTransacao__servidorXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    uvafat_Instancia_p I = uvafat_Instancia_obterImpl_(S);
    uvafat_finalizarTransacao(I, dados);
    uvafat_Instancia_copiarErro_(S, I);
}


/*
 * [[ IMPLEMENTAÇÃO: API INTERNA ]]]
 */


void LOG(uvafat_Instancia_ServidorXmlRpccImpl_* S_, char const* fmt, ...) {
    if (S_->arquivoLog) {
        va_list args;
        va_start(args, fmt);
        vfprintf(S_->arquivoLog, fmt, args);
        va_end(args);
    }
}

void ERR(uvafat_Instancia_ServidorXmlRpccImpl_* S_, char const* fmt, ...) {
    if (S_->arquivoErr) {
        va_list args;
        va_start(args, fmt);
        vfprintf(S_->arquivoErr, fmt, args);
        va_end(args);
    }
}

uvafat_Instancia_p uvafat_Instancia_obterImpl_(uvafat_Instancia_p S) {
    return uvafat_Instancia_obterWrapper_(S)->instancia;
}

uvafat_Instancia_ServidorXmlRpccImpl_* uvafat_Instancia_obterWrapper_(uvafat_Instancia_p S) {
    return (uvafat_Instancia_ServidorXmlRpccImpl_*) S->impl_p;
}

uvafat_Bool uvafat_Instancia_criarServidorXmlRpcc_(
    struct uvafat_Instancia* S,
    uvafat_Instancia_ServidorXmlRpccImpl_* S_,
    uvafat_DadosInstanciaServidor* dados,
    uvafat_IdErro* erro
) {
    xmlrpc_env_init(&S_->env);

    xmlrpc_server_abyss_global_init(&S_->env);
    if (S_->env.fault_occurred) {
        ERR(S_, "*ERRO ao iniciar xmlrpc-c globalmente! Código: %d, Mensagem: %s\n",
                S_->env.fault_code, S_->env.fault_string);
        goto error;
    }

    S_->registry = xmlrpc_registry_new(&S_->env);
    if (S_->env.fault_occurred) {
        ERR(S_, "*ERRO ao iniciar registro de métodos do xmlrpc-c! Código: %d, Mensagem: %s\n",
                S_->env.fault_code, S_->env.fault_string);
        goto error;
    }

    { // Registrando métodos
        struct xmlrpc_method_info3 const uvafat_Instancia_XmlRpccImpl_apiInfo_[] = {
            {
                .methodName      = "uvafat.criarUsuario",
                .methodFunction  = &uvafat_criarUsuario__servidorXmlRpccWrapper_,
                .serverInfo      = S,
                .signatureString = "S:S"
            }, {
                .methodName      = "uvafat.obterDadosUsuario",
                .methodFunction  = &uvafat_obterDadosUsuario__servidorXmlRpccWrapper_,
                .serverInfo      = S,
                .signatureString = "S:S"
            }, {
                .methodName      = "uvafat.adicionarMeioPagamento",
                .methodFunction  = &uvafat_adicionarMeioPagamento__servidorXmlRpccWrapper_,
                .serverInfo      = S,
                .signatureString = "S:S"
            }, {
                .methodName      = "uvafat.listarMeiosPagamento",
                .methodFunction  = &uvafat_listarMeiosPagamento__servidorXmlRpccWrapper_,
                .serverInfo      = S,
                .signatureString = "S:S"
            }, {
                .methodName      = "uvafat.iniciarTransacao",
                .methodFunction  = &uvafat_iniciarTransacao__servidorXmlRpccWrapper_,
                .serverInfo      = S,
                .signatureString = "S:S"
            }, {
                .methodName     = "uvafat.listarTransacoes",
                .methodFunction = &uvafat_listarTransacoes__servidorXmlRpccWrapper_,
                .serverInfo      = S,
                .signatureString = "S:S"
            }
        };

        size_t i;
        size_t sz = sizeof(uvafat_Instancia_XmlRpccImpl_apiInfo_)
               / sizeof(struct xmlrpc_method_info3);
        for (i = 0; i < sz; ++i) {
            struct xmlrpc_method_info3 const* info = &uvafat_Instancia_XmlRpccImpl_apiInfo_[i];
            xmlrpc_registry_add_method3(&S_->env, S_->registry, info);
            if (S_->env.fault_occurred) {
                ERR(S_, "*ERRO ao registrar método '%s' no servidor xmlrpc-c! "
                        "Código: %d, Mensagem: %s\n",
                        info->methodName, S_->env.fault_code, S_->env.fault_string);
                goto error;
            }
        }
    }

    { // Iniciando servidor
        unsigned int numeroPorta = dados->numeroPorta
            ? dados->numeroPorta
            : uvafat_Instancia_porta_STD;
        xmlrpc_server_abyss_parms params;
        params.config_file_name = NULL;   /* Select the modern normal API */
        params.registryP        = S_->registry;
        params.port_number      = numeroPorta;
        params.log_file_name    = dados->nomeArquivoLog;
        params.uri_path         = dados->contextoAplicacao;

        xmlrpc_server_abyss_create(
                &S_->env,
                &params,
                XMLRPC_APSIZE(registryP),
                &S_->server);

        if (S_->env.fault_occurred) {
            ERR(S_, "*ERRO ao iniciar no servidor xmlrpc-c! "
                "Código: %d, Mensagem: %s\n",
                S_->env.fault_code, S_->env.fault_string);
            goto error;
        }

        xmlrpc_server_abyss_setup_sig(&S_->env, S_->server, &S_->oldHandlers);

        if (S_->env.fault_occurred) {
            ERR(S_, "*ERRO ao subbstituir os handlers no servidor xmlrpc-c! "
                "Código: %d, Mensagem: %s\n",
                S_->env.fault_code, S_->env.fault_string);
            goto error;
        }

        xmlrpc_server_abyss_run_server(&S_->env, S_->server);

        if (S_->env.fault_occurred) {
            ERR(S_, "*ERRO ao iniciar no servidor xmlrpc-c! "
                "Código: %d, Mensagem: %s\n",
                S_->env.fault_code, S_->env.fault_string);
            goto error;
        }
    }

    return 1;

error:
    if (S_->oldHandlers) {
        xmlrpc_server_abyss_restore_sig(S_->oldHandlers);
        free(S_->oldHandlers);
    }
    if (S_->server) {
        xmlrpc_server_abyss_destroy(S_->server);
    }
    if (S_->registry) {
        xmlrpc_registry_free(S_->registry);
    }
    return 0;
}

void uvafat_Instancia_finalizarServidorXmlRpcc_(uvafat_Instancia_ServidorXmlRpccImpl_* S_) {

    xmlrpc_server_abyss_terminate(&S_->env, S_->server);
    xmlrpc_server_abyss_restore_sig(S_->oldHandlers);
    free(S_->oldHandlers);
    xmlrpc_server_abyss_destroy(S_->server);
    xmlrpc_registry_free(S_->registry);
    xmlrpc_server_abyss_global_term();
}

XMLRPC_C_DECL(uvafat_criarUsuario__servidorXmlRpccWrapper_) {
    uvafat_Instancia_p S = (uvafat_Instancia_p) serverInfo;
    uvafat_Instancia_ServidorXmlRpccImpl_* S_ = uvafat_Instancia_obterWrapper_(S);
    uvafat_Instancia_p I = S_->instancia;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idErroValue = NULL;
    xmlrpc_value* mensagemErroValue = NULL;
    xmlrpc_value* idUsuarioValue = NULL;
    xmlrpc_value* strukt = NULL;
    xmlrpc_value* nomeUsuarioValue = NULL;

    xmlrpc_value* ret = xmlrpc_struct_new(envP);
    if (!envP->fault_occurred) xmlrpc_array_read_item(envP, paramArrayP, 0, &strukt);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "nomeUsuario", &nomeUsuarioValue);

    char const* nomeUsuario = NULL;

    if (!envP->fault_occurred) xmlrpc_read_string(envP, nomeUsuarioValue, &nomeUsuario);

    if (!envP->fault_occurred) {
        uvafat_DadosUsuario dadosUsuario = {
            .nome = nomeUsuario
        };
        uvafat_IdUsuario idUsuario = uvafat_criarUsuario(I, &dadosUsuario);
        
        if (!uvafat_ok(I)) {
            uvafat_IdErro idErro = uvafat_obterIdErro(I);
            char const* mensagemErro = uvafat_obterMensagemErro(I);
            ERR(S_, "[ERRO] Não foi possível criar usuário! Código: %s, Mensagem: %s\n", idErro, mensagemErro);
            sucessoValue = xmlrpc_bool_new(envP, 0);
            if (!envP->fault_occurred) idErroValue = xmlrpc_int_new(envP, idErro);
            if (!envP->fault_occurred) mensagemErroValue = xmlrpc_string_new(envP, mensagemErro);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idErro", idErroValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "mensagemErro", mensagemErroValue);
            
        } else {
            LOG(S_, "[INFO] Usuário (ID:%d) criado com sucesso.\n", idUsuario);
            sucessoValue = xmlrpc_bool_new(envP, 1);
            if (!envP->fault_occurred) idUsuarioValue = xmlrpc_int_new(envP, idUsuario);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idUsuario", idUsuarioValue);            
        }
    }

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idErroValue) xmlrpc_DECREF(idErroValue);
    if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue);
    if (nomeUsuarioValue) xmlrpc_DECREF(nomeUsuarioValue);
    if (strukt) xmlrpc_DECREF(strukt);
    return ret;
}

XMLRPC_C_DECL(uvafat_obterDadosUsuario__servidorXmlRpccWrapper_) {
    uvafat_Instancia_p S = (uvafat_Instancia_p) serverInfo;
    uvafat_Instancia_ServidorXmlRpccImpl_* S_ = uvafat_Instancia_obterWrapper_(S);
    uvafat_Instancia_p I = S_->instancia;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idErroValue = NULL;
    xmlrpc_value* mensagemErroValue = NULL;
    xmlrpc_value* idUsuarioPesquisaValue = NULL;
    xmlrpc_value* nomeUsuarioPesquisaValue = NULL;
    xmlrpc_value* idUsuarioValue = NULL;
    xmlrpc_value* nomeUsuarioValue = NULL;
    xmlrpc_value* qtdMeiosPagamentoValue = NULL;
    xmlrpc_value* qtdTransacoesValue = NULL;
    xmlrpc_value* strukt = NULL;

    xmlrpc_value* ret = xmlrpc_struct_new(envP);
    xmlrpc_bool possuiIdUsuario = 0;
    xmlrpc_bool possuiNomeUsuario = 0;

    if (!envP->fault_occurred) xmlrpc_array_read_item(envP, paramArrayP, 0, &strukt);
    if (!envP->fault_occurred) possuiIdUsuario = xmlrpc_struct_has_key(envP, strukt, "idUsuario");
    if (!envP->fault_occurred) possuiNomeUsuario = xmlrpc_struct_has_key(envP, strukt, "nomeUsuario");
    if (!envP->fault_occurred && possuiIdUsuario) xmlrpc_struct_find_value(envP, strukt, "idUsuario", &idUsuarioPesquisaValue);
    if (!envP->fault_occurred && possuiNomeUsuario) xmlrpc_struct_find_value(envP, strukt, "nomeUsuario", &nomeUsuarioPesquisaValue);

    uvafat_IdUsuario idUsuarioPesquisa = uvafat_IDNULO;
    char const* nomeUsuarioPesquisa = NULL;

    if (!envP->fault_occurred && idUsuarioPesquisaValue) xmlrpc_read_int(envP, idUsuarioPesquisaValue, &idUsuarioPesquisa);
    if (!envP->fault_occurred && nomeUsuarioPesquisaValue) xmlrpc_read_string(envP, nomeUsuarioPesquisaValue, &nomeUsuarioPesquisa);

    if (!envP->fault_occurred) {
        uvafat_DadosUsuario dadosUsuario = {
            .id = idUsuarioPesquisa,
            .nome = nomeUsuarioPesquisa
        };
        uvafat_obterDadosUsuario(I, &dadosUsuario);

        if (!uvafat_ok(I)) {
            uvafat_IdErro idErro = uvafat_obterIdErro(I);
            char const* mensagemErro = uvafat_obterMensagemErro(I);
            ERR(S_, "[ERRO] Não foi possível obter dados do usuário (ID: %d, Nome: %s)! Código: %d, Mensagem: %s\n", idUsuarioPesquisa, nomeUsuarioPesquisa, idErro, mensagemErro);
            sucessoValue = xmlrpc_bool_new(envP, 0);
            if (!envP->fault_occurred) idErroValue = xmlrpc_int_new(envP, idErro);
            if (!envP->fault_occurred) mensagemErroValue = xmlrpc_string_new(envP, mensagemErro);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idErro", idErroValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "mensagemErro", mensagemErroValue);

        } else {
            LOG(S_, "[INFO] Mostrando dados do usuário com ID %d...\n", idUsuarioPesquisa);
            sucessoValue = xmlrpc_bool_new(envP, 1);
            if (!envP->fault_occurred) idUsuarioValue = xmlrpc_int_new(envP, dadosUsuario.id);
            if (!envP->fault_occurred) nomeUsuarioValue = xmlrpc_string_new(envP, dadosUsuario.nome);
            if (!envP->fault_occurred) qtdMeiosPagamentoValue = xmlrpc_int_new(envP, dadosUsuario.qtdMeiosPagamento);
            if (!envP->fault_occurred) qtdTransacoesValue = xmlrpc_int_new(envP, dadosUsuario.qtdTransacoes);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idUsuario", idUsuarioValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "nomeUsuario", nomeUsuarioValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "qtdMeiosPagamento", qtdMeiosPagamentoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "qtdTransacoes", qtdTransacoesValue);
        }
    }

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idErroValue) xmlrpc_DECREF(idErroValue);
    if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue);
    if (idUsuarioPesquisaValue) xmlrpc_DECREF(idUsuarioPesquisaValue);
    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
    if (nomeUsuarioValue) xmlrpc_DECREF(nomeUsuarioValue);
    if (qtdMeiosPagamentoValue) xmlrpc_DECREF(qtdMeiosPagamentoValue);
    if (qtdTransacoesValue) xmlrpc_DECREF(qtdTransacoesValue);
    if (strukt) xmlrpc_DECREF(strukt);
    return ret;
}

XMLRPC_C_DECL(uvafat_adicionarMeioPagamento__servidorXmlRpccWrapper_) {
    uvafat_Instancia_p S = (uvafat_Instancia_p) serverInfo;
    uvafat_Instancia_ServidorXmlRpccImpl_* S_ = uvafat_Instancia_obterWrapper_(S);
    uvafat_Instancia_p I = S_->instancia;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idErroValue = NULL;
    xmlrpc_value* mensagemErroValue = NULL;
    xmlrpc_value* idUsuarioValue = NULL;
    xmlrpc_value* numeroCartaoValue = NULL;
    xmlrpc_value* cvvCartaoValue = NULL;
    xmlrpc_value* mesVencimentoValue = NULL;
    xmlrpc_value* anoVencimentoValue = NULL;
    xmlrpc_value* idMeioPagamentoValue = NULL;
    xmlrpc_value* strukt = NULL;

    xmlrpc_value* ret = xmlrpc_struct_new(envP);
    if (!envP->fault_occurred) xmlrpc_array_read_item(envP, paramArrayP, 0, &strukt);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "idUsuario", &idUsuarioValue);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "numeroCartao", &numeroCartaoValue);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "cvvCartao", &cvvCartaoValue);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "mesVencimento", &mesVencimentoValue);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "anoVencimento", &anoVencimentoValue);

    uvafat_IdUsuario idUsuario = uvafat_IDNULO;
    char const* numeroCartao = NULL;
    char const* cvvCartao = NULL;
    unsigned int mesVencimento = 0;
    unsigned int anoVencimento = 0;

    if (!envP->fault_occurred) xmlrpc_read_int(envP, idUsuarioValue, &idUsuario);
    if (!envP->fault_occurred) xmlrpc_read_string(envP, numeroCartaoValue, &numeroCartao);
    if (!envP->fault_occurred) xmlrpc_read_string(envP, cvvCartaoValue, &cvvCartao);
    if (!envP->fault_occurred) xmlrpc_read_int(envP, mesVencimentoValue, &mesVencimento);
    if (!envP->fault_occurred) xmlrpc_read_int(envP, anoVencimentoValue, &anoVencimento);

    if (!envP->fault_occurred) {
        uvafat_DadosMeioPagamento dadosMeioPagamento = {
            .idUsuario = idUsuario,
            .numero = numeroCartao,
            .cvv = cvvCartao,
            .mesVencimento = mesVencimento,
            .anoVencimento = anoVencimento
        };
        uvafat_IdMeioPagamento idMeioPagamento = uvafat_adicionarMeioPagamento(I, &dadosMeioPagamento);
        
        if (!uvafat_ok(I)) {
            uvafat_IdErro idErro = uvafat_obterIdErro(I);
            char const* mensagemErro = uvafat_obterMensagemErro(I);
            ERR(S_, "[ERRO] Não foi possível criar meio de pagamento! Código: %s, Mensagem: %s\n", idErro, mensagemErro);
            sucessoValue = xmlrpc_bool_new(envP, 0);
            if (!envP->fault_occurred) idErroValue = xmlrpc_int_new(envP, idErro);
            if (!envP->fault_occurred) mensagemErroValue = xmlrpc_string_new(envP, mensagemErro);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idErro", idErroValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "mensagemErro", mensagemErroValue);
            
        } else {
            LOG(S_, "[INFO] Meio de pagamento (ID:%d) criado com sucesso.\n", idMeioPagamento);
            sucessoValue = xmlrpc_bool_new(envP, 1);
            if (!envP->fault_occurred) idMeioPagamentoValue = xmlrpc_int_new(envP, idMeioPagamento);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idMeioPagamento", idMeioPagamentoValue);
        }
    }

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idErroValue) xmlrpc_DECREF(idErroValue);
    if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue);
    if (numeroCartaoValue) xmlrpc_DECREF(numeroCartaoValue);
    if (cvvCartaoValue) xmlrpc_DECREF(cvvCartaoValue);
    if (mesVencimentoValue) xmlrpc_DECREF(mesVencimentoValue);
    if (anoVencimentoValue) xmlrpc_DECREF(anoVencimentoValue);
    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
    if (strukt) xmlrpc_DECREF(strukt);
    return ret;
}

XMLRPC_C_DECL(uvafat_listarMeiosPagamento__servidorXmlRpccWrapper_) {
    uvafat_Instancia_p S = (uvafat_Instancia_p) serverInfo;
    uvafat_Instancia_ServidorXmlRpccImpl_* S_ = uvafat_Instancia_obterWrapper_(S);
    uvafat_Instancia_p I = S_->instancia;

    // Sucesso/erro da chamada
    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idErroValue = NULL;
    xmlrpc_value* mensagemErroValue = NULL;

    // Entrada
    xmlrpc_value* strukt = NULL;
    xmlrpc_value* idUsuarioValue = NULL;

    // Saída
    xmlrpc_value* meiosPagamentoValue = NULL;
    xmlrpc_value* qtdMeiosPagamentoValue = NULL;

    xmlrpc_value* ret = xmlrpc_struct_new(envP);
    if (!envP->fault_occurred) xmlrpc_array_read_item(envP, paramArrayP, 0, &strukt);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "idUsuario", &idUsuarioValue);

    uvafat_IdUsuario idUsuario = uvafat_IDNULO;

    if (!envP->fault_occurred) xmlrpc_read_int(envP, idUsuarioValue, &idUsuario);

    if (!envP->fault_occurred) {
        size_t qtdTotal = uvafat_listarMeiosPagamento(I, idUsuario, NULL, 0);
        uvafat_DadosMeioPagamento* dados = malloc(sizeof(uvafat_DadosMeioPagamento) * qtdTotal);

        if (dados) {
            size_t qtdCarregados = uvafat_listarMeiosPagamento(I, idUsuario, dados, qtdTotal);

            if (!uvafat_ok(I)) {
                uvafat_IdErro idErro = uvafat_obterIdErro(I);
                char const* mensagemErro = uvafat_obterMensagemErro(I);
                ERR(S_, "[ERRO] Não foi possível listar meios de pagamento para o usuário com ID %d! Código: %s, Mensagem: %s\n", idUsuario, idErro, mensagemErro);
                sucessoValue = xmlrpc_bool_new(envP, 0);
                if (!envP->fault_occurred) idErroValue = xmlrpc_int_new(envP, idErro);
                if (!envP->fault_occurred) mensagemErroValue = xmlrpc_string_new(envP, mensagemErro);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idErro", idErroValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "mensagemErro", mensagemErroValue);

            } else {
                LOG(S_, "[INFO] Listando meios de pagamento para o usuário de ID %d (total: %zd)...\n", idUsuario, qtdCarregados);
                sucessoValue = xmlrpc_bool_new(envP, 1);
                if (!envP->fault_occurred) meiosPagamentoValue = xmlrpc_array_new(envP);
                if (!envP->fault_occurred) qtdMeiosPagamentoValue = xmlrpc_int_new(envP, qtdCarregados);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idUsuario", idUsuarioValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "meiosPagamento", meiosPagamentoValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "qtdMeiosPagamento", qtdMeiosPagamentoValue);

                size_t i;
                for (i = 0; i < qtdCarregados && !envP->fault_occurred; ++i) {
                    uvafat_DadosMeioPagamento const* meioPagamento = &dados[i];

                    xmlrpc_value* idUsuarioValue = NULL;
                    xmlrpc_value* meioPagamentoValue = NULL;
                    xmlrpc_value* idMeioPagamentoValue = NULL;
                    xmlrpc_value* numeroCartaoValue = NULL;
                    xmlrpc_value* cvvCartaoValue = NULL;
                    xmlrpc_value* mesVencimentoValue = NULL;
                    xmlrpc_value* anoVencimentoValue = NULL;

                    if (!envP->fault_occurred) meioPagamentoValue = xmlrpc_struct_new(envP);
                    if (!envP->fault_occurred) idUsuarioValue = xmlrpc_int_new(envP, meioPagamento->idUsuario);
                    if (!envP->fault_occurred) idMeioPagamentoValue = xmlrpc_int_new(envP, meioPagamento->id);
                    if (!envP->fault_occurred) numeroCartaoValue = xmlrpc_string_new(envP, meioPagamento->numero);
                    if (!envP->fault_occurred) cvvCartaoValue = xmlrpc_string_new(envP, meioPagamento->cvv);
                    if (!envP->fault_occurred) mesVencimentoValue = xmlrpc_int_new(envP, meioPagamento->mesVencimento);
                    if (!envP->fault_occurred) anoVencimentoValue = xmlrpc_int_new(envP, meioPagamento->anoVencimento);

                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, meioPagamentoValue, "idUsuario", idMeioPagamentoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, meioPagamentoValue, "idMeioPagamento", idMeioPagamentoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, meioPagamentoValue, "numeroCartao", numeroCartaoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, meioPagamentoValue, "cvvCartao", cvvCartaoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, meioPagamentoValue, "mesVencimento", mesVencimentoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, meioPagamentoValue, "anoVencimento", anoVencimentoValue);

                    if (!envP->fault_occurred) xmlrpc_array_append_item(envP, meiosPagamentoValue, meioPagamentoValue);

                    if (meioPagamentoValue) xmlrpc_DECREF(meioPagamentoValue);
                    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
                    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
                    if (numeroCartaoValue) xmlrpc_DECREF(numeroCartaoValue);
                    if (cvvCartaoValue) xmlrpc_DECREF(cvvCartaoValue);
                    if (mesVencimentoValue) xmlrpc_DECREF(mesVencimentoValue);
                    if (anoVencimentoValue) xmlrpc_DECREF(anoVencimentoValue);
                }
            }

            free(dados);
        }
    }

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idErroValue) xmlrpc_DECREF(idErroValue);
    if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue);

    if (strukt) xmlrpc_DECREF(strukt);
    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);

    if (meiosPagamentoValue) xmlrpc_DECREF(meiosPagamentoValue);
    if (qtdMeiosPagamentoValue) xmlrpc_DECREF(qtdMeiosPagamentoValue);

    return ret;
}

XMLRPC_C_DECL(uvafat_iniciarTransacao__servidorXmlRpccWrapper_) {
    uvafat_Instancia_p S = (uvafat_Instancia_p) serverInfo;
    uvafat_Instancia_ServidorXmlRpccImpl_* S_ = uvafat_Instancia_obterWrapper_(S);
    uvafat_Instancia_p I = S_->instancia;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idErroValue = NULL;
    xmlrpc_value* mensagemErroValue = NULL;
    xmlrpc_value* idMeioPagamentoValue = NULL;
    xmlrpc_value* valorTransacaoValue = NULL;
    xmlrpc_value* idTransacaoValue = NULL;
    xmlrpc_value* structValue = NULL;

    xmlrpc_value* ret = xmlrpc_struct_new(envP);
    if (!envP->fault_occurred) xmlrpc_array_read_item(envP, paramArrayP, 0, &structValue);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, structValue, "idMeioPagamento", &idMeioPagamentoValue);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, structValue, "valor", &valorTransacaoValue);

    uvafat_IdMeioPagamento idMeioPagamento = uvafat_IDNULO;
    double valor = 0;

    if (!envP->fault_occurred) xmlrpc_read_int(envP, idMeioPagamentoValue, &idMeioPagamento);
    if (!envP->fault_occurred) xmlrpc_read_double(envP, valorTransacaoValue, &valor);

    if (!envP->fault_occurred) {
        uvafat_DadosTransacao dadosTransacao = {
            .idMeioPagamento = idMeioPagamento,
            .valor = valor
        };
        uvafat_IdTransacao idTransacao = uvafat_iniciarTransacao(I, &dadosTransacao);

        if (!uvafat_ok(I)) {
            uvafat_IdErro idErro = uvafat_obterIdErro(I);
            char const* mensagemErro = uvafat_obterMensagemErro(I);
            ERR(S_, "[ERRO] Não foi possível iniciar transação! Código: %s, Mensagem: %s\n", idErro, mensagemErro);
            sucessoValue = xmlrpc_bool_new(envP, 0);
            if (!envP->fault_occurred) idErroValue = xmlrpc_int_new(envP, idErro);
            if (!envP->fault_occurred) mensagemErroValue = xmlrpc_string_new(envP, mensagemErro);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idErro", idErroValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "mensagemErro", mensagemErroValue);

        } else {
            LOG(S_, "[INFO] Transação (ID:%d) iniciada com sucesso.\n", idTransacao);
            sucessoValue = xmlrpc_bool_new(envP, 1);
            if (!envP->fault_occurred) idTransacaoValue = xmlrpc_int_new(envP, idTransacao);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
            if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idTransacao", idTransacaoValue);
        }
    }

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idErroValue) xmlrpc_DECREF(idErroValue);
    if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue);
    if (idTransacaoValue) xmlrpc_DECREF(idTransacaoValue);
    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
    if (valorTransacaoValue) xmlrpc_DECREF(valorTransacaoValue);
    if (structValue) xmlrpc_DECREF(structValue);
    return ret;
}

XMLRPC_C_DECL(uvafat_listarTransacoes__servidorXmlRpccWrapper_) {
    uvafat_Instancia_p S = (uvafat_Instancia_p) serverInfo;
    uvafat_Instancia_ServidorXmlRpccImpl_* S_ = uvafat_Instancia_obterWrapper_(S);
    uvafat_Instancia_p I = S_->instancia;

    // Sucesso/erro da chamada
    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idErroValue = NULL;
    xmlrpc_value* mensagemErroValue = NULL;

    // Entrada
    xmlrpc_value* strukt = NULL;
    xmlrpc_value* idUsuarioValue = NULL;

    // Saída
    xmlrpc_value* transacoesValue = NULL;
    xmlrpc_value* qtdTransacoesValue = NULL;

    xmlrpc_value* ret = xmlrpc_struct_new(envP);
    if (!envP->fault_occurred) xmlrpc_array_read_item(envP, paramArrayP, 0, &strukt);
    if (!envP->fault_occurred) xmlrpc_struct_find_value(envP, strukt, "idUsuario", &idUsuarioValue);

    uvafat_IdUsuario idUsuario = uvafat_IDNULO;

    if (!envP->fault_occurred) xmlrpc_read_int(envP, idUsuarioValue, &idUsuario);

    if (!envP->fault_occurred) {
        size_t qtdTotal = uvafat_listarTransacoes(I, idUsuario, NULL, 0);
        uvafat_DadosTransacao* dados = malloc(sizeof(uvafat_DadosTransacao) * qtdTotal);

        if (dados) {
            size_t qtdCarregados = uvafat_listarTransacoes(I, idUsuario, dados, qtdTotal);

            if (!uvafat_ok(I)) {
                uvafat_IdErro idErro = uvafat_obterIdErro(I);
                char const* mensagemErro = uvafat_obterMensagemErro(I);
                ERR(S_, "[ERRO] Não foi possível listar as transações do usuário com ID %d! Código: %s, Mensagem: %s\n", idUsuario, idErro, mensagemErro);
                sucessoValue = xmlrpc_bool_new(envP, 0);
                if (!envP->fault_occurred) idErroValue = xmlrpc_int_new(envP, uvafat_obterIdErro(S));
                if (!envP->fault_occurred) mensagemErroValue = xmlrpc_string_new(envP, uvafat_obterMensagemErro(S));
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idErro", idErroValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "mensagemErro", mensagemErroValue);

            } else {
                LOG(S_, "[INFO] Listando transações do usuário com ID %d (total: %zd)...\n", idUsuario, qtdCarregados);
                sucessoValue = xmlrpc_bool_new(envP, 1);
                if (!envP->fault_occurred) transacoesValue = xmlrpc_array_new(envP);
                if (!envP->fault_occurred) qtdTransacoesValue = xmlrpc_int_new(envP, qtdCarregados);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "sucesso", sucessoValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "idUsuario", idUsuarioValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "transacoes", transacoesValue);
                if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, ret, "qtdTransacoes", qtdTransacoesValue);

                size_t i;
                for (i = 0; i < qtdCarregados && !envP->fault_occurred; ++i) {
                    uvafat_DadosTransacao const* transacao = &dados[i];
                    char const* nomeEstado = uvafat_obterSimboloEstadoTransacao(transacao->estado);
                    uvafat_Bool terminal = uvafat_isEstadoTransacaoTerminal(transacao->estado);
                    xmlrpc_datetime const dataInicio = {
                        .Y = transacao->dataInicio.tm_year + 1900,
                        .M = transacao->dataInicio.tm_mon + 1,
                        .D = transacao->dataInicio.tm_mday,
                        .h = transacao->dataInicio.tm_hour,
                        .m = transacao->dataInicio.tm_min,
                        .s = transacao->dataInicio.tm_sec,
                        .u = 0
                    };
                    xmlrpc_datetime const dataTermino = {
                        .Y = transacao->dataTermino.tm_year + 1900,
                        .M = transacao->dataTermino.tm_mon + 1,
                        .D = transacao->dataTermino.tm_mday,
                        .h = transacao->dataTermino.tm_hour,
                        .m = transacao->dataTermino.tm_min,
                        .s = transacao->dataTermino.tm_sec,
                        .u = 0
                    };

                    xmlrpc_value* transacaoValue = NULL;
                    xmlrpc_value* idUsuarioValue = NULL;
                    xmlrpc_value* idTransacaoValue = NULL;
                    xmlrpc_value* idMeioPagamentoValue = NULL;
                    xmlrpc_value* idEstadoValue = NULL;
                    xmlrpc_value* nomeEstadoValue = NULL;
                    xmlrpc_value* terminadoValue = NULL;
                    xmlrpc_value* valorValue = NULL;
                    xmlrpc_value* dataInicioValue = NULL;
                    xmlrpc_value* dataTerminoValue = NULL;

                    if (!envP->fault_occurred) transacaoValue = xmlrpc_struct_new(envP);
                    if (!envP->fault_occurred) idTransacaoValue = xmlrpc_int_new(envP, transacao->id);
                    if (!envP->fault_occurred) idUsuarioValue = xmlrpc_int_new(envP, transacao->idUsuario);
                    if (!envP->fault_occurred) idMeioPagamentoValue = xmlrpc_int_new(envP, transacao->idMeioPagamento);
                    if (!envP->fault_occurred) idEstadoValue = xmlrpc_int_new(envP, transacao->estado);
                    if (!envP->fault_occurred) nomeEstadoValue = xmlrpc_string_new(envP, nomeEstado);
                    if (!envP->fault_occurred) terminadoValue = xmlrpc_bool_new(envP, terminal);
                    if (!envP->fault_occurred) valorValue = xmlrpc_double_new(envP, transacao->valor);
                    if (!envP->fault_occurred) dataInicioValue = xmlrpc_datetime_new(envP, dataInicio);
                    if (!envP->fault_occurred) {
                        if (terminal) dataTerminoValue = xmlrpc_datetime_new(envP, dataTermino);
                        else dataTerminoValue = xmlrpc_nil_new(envP);
                    }

                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "idUsuario", idUsuarioValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "idTransacao", idTransacaoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "idMeioPagamento", idMeioPagamentoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "idEstado", idEstadoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "nomeEstado", nomeEstadoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "terminado", terminadoValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "valor", valorValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "dataInicio", dataInicioValue);
                    if (!envP->fault_occurred) xmlrpc_struct_set_value(envP, transacaoValue, "dataTermino", dataTerminoValue);

                    if (!envP->fault_occurred) xmlrpc_array_append_item(envP, transacoesValue, transacaoValue);

                    if (transacaoValue) xmlrpc_DECREF(transacaoValue);
                    if (idTransacaoValue) xmlrpc_DECREF(idTransacaoValue);
                    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
                    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
                    if (idEstadoValue) xmlrpc_DECREF(idEstadoValue);
                    if (nomeEstadoValue) xmlrpc_DECREF(nomeEstadoValue);
                    if (terminadoValue) xmlrpc_DECREF(terminadoValue);
                    if (valorValue) xmlrpc_DECREF(valorValue);
                    if (dataInicioValue) xmlrpc_DECREF(dataInicioValue);
                    if (dataTerminoValue) xmlrpc_DECREF(dataTerminoValue);
                }
            }

            free(dados);
        }
    }

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idErroValue) xmlrpc_DECREF(idErroValue);
    if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue);

    if (strukt) xmlrpc_DECREF(strukt);
    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);

    if (transacoesValue) xmlrpc_DECREF(transacoesValue);
    if (qtdTransacoesValue) xmlrpc_DECREF(qtdTransacoesValue);

    return ret;
}
