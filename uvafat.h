// $flisboac 2017-10-22
/** 
 * @file uvafat.h
 */
#ifndef UVA_SD_TRABALHO1_ITEM2_UVAFAT_H__
#define UVA_SD_TRABALHO1_ITEM2_UVAFAT_H__

#include <stddef.h>
#include <stdint.h>
#include <time.h>
#include <stdio.h>

#define uvafat_Bool char

#define uvafat_Usuario_nome_MAX 33
#define uvafat_MeioPagamento_numero_MAX 17
#define uvafat_MeioPagamento_cvv_MAX 4

#define uvafat_DadosInstanciaServidor_contextoAplicacao_MAX 64

typedef enum uvafat_IdErro uvafat_IdErro;

typedef int uvafat_Id;
#define uvafat_IDNULO 0
#define uvafat_IDP "%d"

typedef struct uvafat_Instancia *uvafat_Instancia_p;
typedef struct uvafat_DadosInstanciaCliente uvafat_DadosInstanciaCliente;
typedef struct uvafat_DadosInstanciaServidor uvafat_DadosInstanciaServidor;

typedef uvafat_Id uvafat_IdUsuario;
typedef struct uvafat_DadosUsuario uvafat_DadosUsuario;

typedef uvafat_Id uvafat_IdMeioPagamento;
typedef struct uvafat_DadosMeioPagamento uvafat_DadosMeioPagamento;

typedef uvafat_Id uvafat_IdTransacao;
typedef enum uvafat_IdEstadoTransacao  uvafat_IdEstadoTransacao;
typedef struct uvafat_DadosTransacao uvafat_DadosTransacao;


/*
 * [[[ ENUMERAÇÕES ]]]
 */


enum uvafat_IdErro {
    uvafat_ERR_OK = 0,
    uvafat_ERRO,
    uvafat_ERR_MEM,
    uvafat_ERR_IDNULO,
    uvafat_ERR_IDINVALIDO,
    uvafat_ERR_NOMENULO,
    uvafat_ERR_CARTAOINVALIDO,
    uvafat_ERR_CVVINVALIDO,
    uvafat_ERR_CARTAOVENCIDO,
    uvafat_ERR_VENCIMENTOINVALIDO,
    uvafat_ERR_VALORINVALIDO,
    uvafat_ERR_FINALIZADO,
    uvafat_ERR_NOVOESTADOINVALIDO,
    uvafat_ERR_TRANSMISSAO,
    uvafat_ERR_NAOSUPORTADO
};

enum uvafat_IdEstadoTransacao {
    uvafat_Transacao_INICIADA,
    uvafat_Transacao_APROVADA,
    uvafat_Transacao_REJEITADA
};

/*
 * [[[ ESTRUTURAS DE DADOS ]]]
 */


struct uvafat_DadosInstanciaCliente {
    //char const* chaveApi;
    char const* endpointUrl;
    FILE* arquivoLog;
    FILE* arquivoErro;
};

struct uvafat_DadosInstanciaServidor {
    //char const* chaveApiGlobal;
    char const* contextoAplicacao;
    unsigned int numeroPorta;
    FILE* arquivoLog;
    FILE* arquivoErro;
    char const* nomeArquivoLog;
};

struct uvafat_DadosUsuario {
    uvafat_IdUsuario id;
    size_t qtdMeiosPagamento;
    size_t qtdTransacoes;
    char const* nome;
    char nome_[uvafat_Usuario_nome_MAX];
};

struct uvafat_DadosMeioPagamento {
    uvafat_IdMeioPagamento id;
    uvafat_IdUsuario idUsuario;
    char const* numero;
    char const* cvv;
    unsigned int anoVencimento;
    unsigned int mesVencimento;
    char numero_[uvafat_MeioPagamento_numero_MAX];
    char cvv_[uvafat_MeioPagamento_cvv_MAX];
};

struct uvafat_DadosTransacao {
    uvafat_IdTransacao id;
    uvafat_IdUsuario idUsuario;
    uvafat_IdMeioPagamento idMeioPagamento;
    uvafat_IdEstadoTransacao estado;
    double valor;
    struct tm dataInicio;
    struct tm dataTermino;
};


/*
 * [[[ FUNÇÕES ]]]
 */


uvafat_Instancia_p uvafat_criar(uvafat_IdErro* erro);
uvafat_Instancia_p uvafat_iniciarServidor(uvafat_DadosInstanciaServidor* dados, uvafat_IdErro* erro);
uvafat_Instancia_p uvafat_conectar(uvafat_DadosInstanciaCliente* dados, uvafat_IdErro* erro);
void uvafat_finalizar(uvafat_Instancia_p S);
uvafat_Bool uvafat_ok(uvafat_Instancia_p S);
uvafat_IdErro uvafat_obterIdErro(uvafat_Instancia_p S);
char const* uvafat_obterMensagemErro(uvafat_Instancia_p S);

uvafat_IdUsuario uvafat_criarUsuario(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);
void uvafat_obterDadosUsuario(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);

uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados);
size_t uvafat_listarMeiosPagamento(uvafat_Instancia_p S, uvafat_IdUsuario usuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados);

uvafat_IdTransacao uvafat_iniciarTransacao(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);
size_t uvafat_listarTransacoes(uvafat_Instancia_p S, uvafat_IdUsuario usuario, uvafat_DadosTransacao* dados, size_t qtdDados);
void uvafat_finalizarTransacao(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);

char const* uvafat_obterSimboloEstadoTransacao(uvafat_IdEstadoTransacao estado);
uvafat_Bool uvafat_isEstadoTransacaoTerminal(uvafat_IdEstadoTransacao estado);

#endif //UVA_SD_TRABALHO1_ITEM2_UVAFAT_H__
