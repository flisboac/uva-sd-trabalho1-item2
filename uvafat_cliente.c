// $flisboac 2017-10-22

#include <stdlib.h>

#include "uvafat_xmlrpcc_.h"
#include <xmlrpc-c/client.h>
#include <xmlrpc-c/base.h>


/*
 * [[ DEFINIÇÕES ]]
 */

#define CAPACIDADE_URL 256
#define VERIFICAR_MOSTRAR_ERRO_XMLRPC \
    do { if (S_->env.fault_occurred) \
        uvafat_Instancia_atribuirMensagemErro_(S, \
            uvafat_ERR_TRANSMISSAO, \
            "Erro ao efetuar chamada remota. Código XMLRPC-C: %d, Mensagem XMLRPC-C: %s", \
            S_->env.fault_code, \
            S_->env.fault_string); } while (0)

#define OBTER_ERRO_CHAMADA \
    do { \
        xmlrpc_value* idErroValue = NULL; \
        xmlrpc_value* mensagemErroValue = NULL; \
        uvafat_IdErro idErro = uvafat_ERRO; \
        char const* mensagemErro = NULL; \
        if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idErroValue, (int*) &idErro); \
        if (!S_->env.fault_occurred) xmlrpc_read_string(&S_->env, mensagemErroValue, &mensagemErro); \
        uvafat_Instancia_atribuirErro_(S, idErro, mensagemErro); \
        if (idErroValue) xmlrpc_DECREF(idErroValue); \
        if (mensagemErroValue) xmlrpc_DECREF(mensagemErroValue); \
    } while (0)

typedef struct uvafat_Instancia_ClienteXmlRpccImpl_ uvafat_Instancia_ClienteXmlRpccImpl_;


struct uvafat_Instancia_ClienteXmlRpccImpl_ {
    FILE* arquivoLog;
    FILE* arquivoErr;
    xmlrpc_client* client;
    char const* url;
    xmlrpc_server_info * serverInfo;
    xmlrpc_env env;
    char url_[CAPACIDADE_URL];
};


/*
 * [[ FUNÇÕES ]]
 */


// API externa

static void uvafat_finalizar__clienteXmlRpccImpl_(uvafat_Instancia_p S);

static uvafat_IdUsuario uvafat_criarUsuario__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);
static void uvafat_obterDadosUsuario__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);

static uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados);
static size_t uvafat_listarMeiosPagamento__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados);

static uvafat_IdTransacao uvafat_iniciarTransacao__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);
static size_t uvafat_listarTransacoes__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosTransacao* dados, size_t qtdDados);
static void uvafat_finalizarTransacao__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);


// API interna

static void LOG(uvafat_Instancia_ClienteXmlRpccImpl_* S_, char const* fmt, ...);
static void ERR(uvafat_Instancia_ClienteXmlRpccImpl_* S_, char const* fmt, ...);
static uvafat_Instancia_ClienteXmlRpccImpl_* uvafat_Instancia_obterImpl_(uvafat_Instancia_p S);
static uvafat_Bool uvafat_Instancia_criarClienteXmlRpcc_(uvafat_Instancia_p S, uvafat_Instancia_ClienteXmlRpccImpl_* S_, uvafat_DadosInstanciaCliente* dados, uvafat_IdErro* erro);
static void uvafat_Instancia_finalizarClienteXmlRpcc_(uvafat_Instancia_ClienteXmlRpccImpl_* S_);


/*
 * [[ IMPLEMENTAÇÃO: API PÚBLICA ]]
 */


static const uvafat_Instancia_Api_ uvafat_instancia__clienteXmlRpccApiImpl_ = {
        uvafat_finalizar__clienteXmlRpccImpl_,

        uvafat_criarUsuario__clienteXmlRpccImpl_,
        uvafat_obterDadosUsuario__clienteXmlRpccImpl_,

        uvafat_adicionarMeioPagamento__clienteXmlRpccImpl_,
        uvafat_listarMeiosPagamento__clienteXmlRpccImpl_,

        uvafat_iniciarTransacao__clienteXmlRpccImpl_,
        uvafat_listarTransacoes__clienteXmlRpccImpl_,
        uvafat_finalizarTransacao__clienteXmlRpccImpl_
};

uvafat_Instancia_p uvafat_conectar(uvafat_DadosInstanciaCliente* dados, uvafat_IdErro* erro) {
    uvafat_IdErro resultado = uvafat_ERR_OK;
    uvafat_Instancia_p S = malloc(sizeof(struct uvafat_Instancia) + sizeof(uvafat_Instancia_ClienteXmlRpccImpl_));

    if (S) {
        uvafat_Instancia_ClienteXmlRpccImpl_* S_ = (void*) S++;
        S->impl_c = &uvafat_instancia__clienteXmlRpccApiImpl_;
        S->impl_p = S_;
        uvafat_Instancia_limparErro_(S);

        S_->arquivoLog = dados->arquivoLog;
        S_->arquivoErr = dados->arquivoErro;
        S_->client = NULL;
        S_->url_[0] = '\0'; strncat(S_->url_, dados->endpointUrl, CAPACIDADE_URL); S_->url = S_->url_;

        if (!uvafat_Instancia_criarClienteXmlRpcc_(S, S_, dados, &resultado)) {
            free(S);
        }

    } else {
        resultado = uvafat_ERR_MEM;
    }

    if (erro) *erro = resultado;
    return S;
}


// -------


void uvafat_finalizar__clienteXmlRpccImpl_(uvafat_Instancia_p S) {
    if (S) {
        uvafat_Instancia_finalizarClienteXmlRpcc_(uvafat_Instancia_obterImpl_(S));
        free(S);
    }
}

uvafat_IdUsuario uvafat_criarUsuario__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    uvafat_Instancia_ClienteXmlRpccImpl_* S_ = uvafat_Instancia_obterImpl_(S);
    uvafat_IdUsuario idUsuario = uvafat_IDNULO;

    xmlrpc_value* idUsuarioValue = NULL;
    xmlrpc_value* resultadoValue = NULL;
    xmlrpc_value* argumentosValue = NULL;
    xmlrpc_value* paramsStructValue = NULL;
    xmlrpc_value* nomeUsuarioValue = NULL;

    xmlrpc_value* sucessoValue = NULL;

    // Criando valores intermediários da chamada
    // xmlrpc_env_clean(&S_->env);
    argumentosValue = xmlrpc_array_new(&S_->env);
    if (!S_->env.fault_occurred) paramsStructValue = xmlrpc_struct_new(&S_->env);
    if (!S_->env.fault_occurred) nomeUsuarioValue = xmlrpc_string_new(&S_->env, dados->nome);

    // Construindo parâmetros da chamada
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "nomeUsuario", nomeUsuarioValue);

    if (!S_->env.fault_occurred) xmlrpc_array_append_item(&S_->env, argumentosValue, paramsStructValue);
    
    // Realização da chamada
    uvafat_Bool sucesso = 0;
    if (!S_->env.fault_occurred) xmlrpc_client_call2(&S_->env, S_->client, S_->serverInfo, "uvafat.criarUsuario", argumentosValue, &resultadoValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "sucesso", &sucessoValue);
    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, sucessoValue, (xmlrpc_bool*) &sucesso);

    if (!S_->env.fault_occurred) {
        if (!sucesso) {
            OBTER_ERRO_CHAMADA;

        } else {
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "idUsuario", &idUsuarioValue);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idUsuarioValue, &idUsuario);
        }
    }
    
    VERIFICAR_MOSTRAR_ERRO_XMLRPC;
    
    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
    if (resultadoValue) xmlrpc_DECREF(resultadoValue);
    if (argumentosValue) xmlrpc_DECREF(argumentosValue);
    if (paramsStructValue) xmlrpc_DECREF(paramsStructValue);
    if (nomeUsuarioValue) xmlrpc_DECREF(nomeUsuarioValue);
    
    return idUsuario;
}

void uvafat_obterDadosUsuario__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    uvafat_Instancia_ClienteXmlRpccImpl_* S_ = uvafat_Instancia_obterImpl_(S);

    xmlrpc_value* idUsuarioPesquisaValue = NULL;
    xmlrpc_value* nomeUsuarioPesquisaValue = NULL;
    xmlrpc_value* resultadoValue = NULL;
    xmlrpc_value* argumentosValue = NULL;
    xmlrpc_value* paramsStructValue = NULL;

    xmlrpc_value* sucessoValue = NULL;

    xmlrpc_value* idUsuarioValue = NULL;
    xmlrpc_value* nomeUsuarioValue = NULL;
    xmlrpc_value* qtdMeiosPagamentoValue = NULL;
    xmlrpc_value* qtdTransacoesValue = NULL;


    // Criando valores intermediários da chamada
    // xmlrpc_env_clean(&S_->env);
    argumentosValue = xmlrpc_array_new(&S_->env);
    if (!S_->env.fault_occurred) paramsStructValue = xmlrpc_struct_new(&S_->env);
    if (!S_->env.fault_occurred && dados->id != uvafat_IDNULO) idUsuarioPesquisaValue = xmlrpc_int_new(&S_->env, dados->id);
    if (!S_->env.fault_occurred && dados->nome != NULL) nomeUsuarioPesquisaValue = xmlrpc_string_new(&S_->env, dados->nome);

    // Construindo parâmetros da chamada
    if (!S_->env.fault_occurred && idUsuarioPesquisaValue) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "idUsuario", idUsuarioPesquisaValue);
    if (!S_->env.fault_occurred && nomeUsuarioPesquisaValue) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "nomeUsuario", nomeUsuarioPesquisaValue);

    if (!S_->env.fault_occurred) xmlrpc_array_append_item(&S_->env, argumentosValue, paramsStructValue);

    // Realização da chamada
    uvafat_Bool sucesso = 0;
    if (!S_->env.fault_occurred) xmlrpc_client_call2(&S_->env, S_->client, S_->serverInfo, "uvafat.obterDadosUsuario", argumentosValue, &resultadoValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "sucesso", &sucessoValue);
    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, sucessoValue, (xmlrpc_bool*) &sucesso);

    if (!S_->env.fault_occurred) {
        if (!sucesso) {
            OBTER_ERRO_CHAMADA;

        } else {
            uvafat_IdUsuario idUsuario = uvafat_IDNULO;
            char const* nomeUsuario = NULL;
            size_t qtdMeiosPagamento = 0;
            size_t qtdTransacoes = 0;
            
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "idUsuario", &idUsuarioValue);
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "nomeUsuario", &nomeUsuarioValue);
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "qtdMeiosPagamento", &qtdMeiosPagamentoValue);
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "qtdTransacoes", &qtdTransacoesValue);
            
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idUsuarioValue, &idUsuario);
            if (!S_->env.fault_occurred) xmlrpc_read_string(&S_->env, nomeUsuarioValue, &nomeUsuario);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, qtdMeiosPagamentoValue, (int*) &qtdMeiosPagamento);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, qtdTransacoesValue, (int*) &qtdTransacoes);
            
            if (!S_->env.fault_occurred) {
                dados->id = idUsuario;
                dados->nome = nomeUsuario;
                dados->qtdTransacoes = qtdTransacoes;
                dados->qtdMeiosPagamento = qtdMeiosPagamento;
            }
        }
    }

    VERIFICAR_MOSTRAR_ERRO_XMLRPC;

    if (idUsuarioPesquisaValue) xmlrpc_DECREF(idUsuarioPesquisaValue);
    if (nomeUsuarioPesquisaValue) xmlrpc_DECREF(nomeUsuarioPesquisaValue);
    if (resultadoValue) xmlrpc_DECREF(resultadoValue);
    if (argumentosValue) xmlrpc_DECREF(argumentosValue);
    if (paramsStructValue) xmlrpc_DECREF(paramsStructValue);

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);

    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
    if (nomeUsuarioValue) xmlrpc_DECREF(nomeUsuarioValue);
    if (qtdMeiosPagamentoValue) xmlrpc_DECREF(qtdMeiosPagamentoValue);
    if (qtdTransacoesValue) xmlrpc_DECREF(qtdTransacoesValue);
}

uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados) {
    uvafat_Instancia_ClienteXmlRpccImpl_* S_ = uvafat_Instancia_obterImpl_(S);
    uvafat_IdMeioPagamento idMeioPagamento = uvafat_IDNULO;

    xmlrpc_value* resultadoValue = NULL;
    xmlrpc_value* argumentosValue = NULL;
    xmlrpc_value* paramsStructValue = NULL;
    
    xmlrpc_value* idUsuarioParamValue = NULL;
    xmlrpc_value* numeroCartaoParamValue = NULL;
    xmlrpc_value* cvvCartaoParamValue = NULL;
    xmlrpc_value* mesVencimentoParamValue = NULL;
    xmlrpc_value* anoVencimentoParamValue = NULL;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idMeioPagamentoValue = NULL;

    // Criando valores intermediários da chamada
    // xmlrpc_env_clean(&S_->env);
    argumentosValue = xmlrpc_array_new(&S_->env);
    if (!S_->env.fault_occurred) paramsStructValue = xmlrpc_struct_new(&S_->env);
    if (!S_->env.fault_occurred) idUsuarioParamValue = xmlrpc_int_new(&S_->env, dados->idUsuario);
    if (!S_->env.fault_occurred) numeroCartaoParamValue = xmlrpc_string_new(&S_->env, dados->numero);
    if (!S_->env.fault_occurred) cvvCartaoParamValue = xmlrpc_string_new(&S_->env, dados->cvv);
    if (!S_->env.fault_occurred) mesVencimentoParamValue = xmlrpc_int_new(&S_->env, dados->mesVencimento);
    if (!S_->env.fault_occurred) anoVencimentoParamValue = xmlrpc_int_new(&S_->env, dados->anoVencimento);

    // Construindo parâmetros da chamada
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "idUsuario", idUsuarioParamValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "numeroCartao", numeroCartaoParamValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "cvvCartao", cvvCartaoParamValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "mesVencimento", mesVencimentoParamValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "anoVencimento", anoVencimentoParamValue);

    if (!S_->env.fault_occurred) xmlrpc_array_append_item(&S_->env, argumentosValue, paramsStructValue);
    
    // Realização da chamada
    uvafat_Bool sucesso = 0;
    if (!S_->env.fault_occurred) xmlrpc_client_call2(&S_->env, S_->client, S_->serverInfo, "uvafat.adicionarMeioPagamento", argumentosValue, &resultadoValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "sucesso", &sucessoValue);
    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, sucessoValue, (xmlrpc_bool*) &sucesso);

    if (!S_->env.fault_occurred) {
        if (!sucesso) {
            OBTER_ERRO_CHAMADA;

        } else {
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "idMeioPagamento", &idMeioPagamentoValue);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idMeioPagamentoValue, &idMeioPagamento);
        }
    }
    
    VERIFICAR_MOSTRAR_ERRO_XMLRPC;
    
    if (resultadoValue) xmlrpc_DECREF(resultadoValue);
    if (argumentosValue) xmlrpc_DECREF(argumentosValue);
    if (paramsStructValue) xmlrpc_DECREF(paramsStructValue);

    if (idUsuarioParamValue) xmlrpc_DECREF(idUsuarioParamValue);
    if (numeroCartaoParamValue) xmlrpc_DECREF(numeroCartaoParamValue);
    if (cvvCartaoParamValue) xmlrpc_DECREF(cvvCartaoParamValue);
    if (mesVencimentoParamValue) xmlrpc_DECREF(mesVencimentoParamValue);
    if (anoVencimentoParamValue) xmlrpc_DECREF(anoVencimentoParamValue);

    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
    
    return idMeioPagamento;
}

size_t uvafat_listarMeiosPagamento__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados) {
    uvafat_Instancia_ClienteXmlRpccImpl_* S_ = uvafat_Instancia_obterImpl_(S);
    size_t qtdEncontrada = 0;
    
    if (dados == NULL) {
        uvafat_DadosUsuario dadosUsuario = { .id = idUsuario, .qtdMeiosPagamento = 0 };
        uvafat_obterDadosUsuario__clienteXmlRpccImpl_(S, &dadosUsuario);
        return dadosUsuario.qtdMeiosPagamento;
    }
    
    xmlrpc_value* idUsuarioPesquisaValue = NULL;
    xmlrpc_value* resultadoValue = NULL;
    xmlrpc_value* argumentosValue = NULL;
    xmlrpc_value* paramsStructValue = NULL;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* qtdMeiosPagamentoValue = NULL;
    xmlrpc_value* meiosPagamentoValue = NULL;

    // Criando valores intermediários da chamada
    // xmlrpc_env_clean(&S_->env);
    argumentosValue = xmlrpc_array_new(&S_->env);
    if (!S_->env.fault_occurred) paramsStructValue = xmlrpc_struct_new(&S_->env);
    if (!S_->env.fault_occurred) idUsuarioPesquisaValue = xmlrpc_int_new(&S_->env, idUsuario);

    // Construindo parâmetros da chamada
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "idUsuario", idUsuarioPesquisaValue);

    if (!S_->env.fault_occurred) xmlrpc_array_append_item(&S_->env, argumentosValue, paramsStructValue);

    // Realização da chamada
    uvafat_Bool sucesso = 0;
    if (!S_->env.fault_occurred) xmlrpc_client_call2(&S_->env, S_->client, S_->serverInfo, "uvafat.listarMeiosPagamento", argumentosValue, &resultadoValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "sucesso", &sucessoValue);
    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, sucessoValue, (xmlrpc_bool*) &sucesso);

    if (!S_->env.fault_occurred) {
        if (!sucesso) {
            OBTER_ERRO_CHAMADA;

        } else {
            size_t qtdMeiosPagamento = 0;
            size_t meiosPagamentoValue_size = 0;
            
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "qtdMeiosPagamento", &qtdMeiosPagamentoValue);
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "meiosPagamento", &meiosPagamentoValue);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, qtdMeiosPagamentoValue, (int*) &qtdMeiosPagamento);
            if (!S_->env.fault_occurred) meiosPagamentoValue_size = (size_t) xmlrpc_array_size(&S_->env, meiosPagamentoValue);
            
            if (qtdMeiosPagamento != meiosPagamentoValue_size) LOG(S_, "[WARN] Tamanho da lista de meios de pagamento (%d) não coincide com o tamanho informado no objeto de resposta (%d).\n", meiosPagamentoValue_size, qtdMeiosPagamento);
            
            if (!S_->env.fault_occurred) {
                size_t i;
                for (i = 0; i < meiosPagamentoValue_size && i < qtdDados && !S_->env.fault_occurred; ++i) {
                    xmlrpc_value* meioPagamentoValue = NULL;
                    xmlrpc_value* idMeioPagamentoValue = NULL;
                    xmlrpc_value* idUsuarioValue = NULL;
                    xmlrpc_value* numeroCartaoValue = NULL;
                    xmlrpc_value* cvvCartaoValue = NULL;
                    xmlrpc_value* mesVencimentoValue = NULL;
                    xmlrpc_value* anoVencimentoValue = NULL;
                    
                    uvafat_IdUsuario idUsuario_ = uvafat_IDNULO;
                    uvafat_IdMeioPagamento idMeioPagamento = uvafat_IDNULO;
                    char const* numeroCartao = NULL;
                    char const* cvvCartao = NULL;
                    unsigned int anoVencimento = 0;
                    unsigned int mesVencimento = 0;
                    
                    if (!S_->env.fault_occurred) xmlrpc_array_read_item(&S_->env, meiosPagamentoValue, (unsigned int) i, &meioPagamentoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, meioPagamentoValue, "idMeioPagamento", &idMeioPagamentoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, meioPagamentoValue, "idUsuario", &idUsuarioValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, meioPagamentoValue, "numeroCartao", &numeroCartaoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, meioPagamentoValue, "cvvCartao", &cvvCartaoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, meioPagamentoValue, "mesVencimento", &mesVencimentoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, meioPagamentoValue, "anoVencimento", &anoVencimentoValue);
                    
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idMeioPagamentoValue, &idMeioPagamento);
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idUsuarioValue, &idUsuario_);
                    if (!S_->env.fault_occurred) xmlrpc_read_string(&S_->env, numeroCartaoValue, &numeroCartao);
                    if (!S_->env.fault_occurred) xmlrpc_read_string(&S_->env, cvvCartaoValue, &cvvCartao);
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, mesVencimentoValue, &mesVencimento);
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, anoVencimentoValue, &anoVencimento);

                    if (!S_->env.fault_occurred) {
                        uvafat_DadosMeioPagamento* meioPagamento = &dados[i];
                        meioPagamento->id = idMeioPagamento;
                        meioPagamento->idUsuario = idUsuario_;
                        meioPagamento->mesVencimento = mesVencimento;
                        meioPagamento->anoVencimento = anoVencimento;
                        meioPagamento->numero = meioPagamento->numero_; meioPagamento->numero_[0] = '\0'; strncat(meioPagamento->numero_, numeroCartao, uvafat_MeioPagamento_numero_MAX);
                        meioPagamento->cvv = meioPagamento->cvv_; meioPagamento->cvv_[0] = '\0'; strncat(meioPagamento->cvv_, cvvCartao, uvafat_MeioPagamento_cvv_MAX); ++qtdEncontrada;
                    }
                    if (meioPagamentoValue) xmlrpc_DECREF(meioPagamentoValue);
                    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
                    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
                    if (numeroCartaoValue) xmlrpc_DECREF(numeroCartaoValue);
                    if (cvvCartaoValue) xmlrpc_DECREF(cvvCartaoValue);
                    if (mesVencimentoValue) xmlrpc_DECREF(mesVencimentoValue);
                    if (anoVencimentoValue) xmlrpc_DECREF(anoVencimentoValue);
                }
            }
        }
    }

    VERIFICAR_MOSTRAR_ERRO_XMLRPC;

    if (idUsuarioPesquisaValue) xmlrpc_DECREF(idUsuarioPesquisaValue);
    if (resultadoValue) xmlrpc_DECREF(resultadoValue);
    if (argumentosValue) xmlrpc_DECREF(argumentosValue);
    if (paramsStructValue) xmlrpc_DECREF(paramsStructValue);
    
    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (qtdMeiosPagamentoValue) xmlrpc_DECREF(qtdMeiosPagamentoValue);
    if (meiosPagamentoValue) xmlrpc_DECREF(meiosPagamentoValue);
    
    return qtdEncontrada;
}

uvafat_IdTransacao uvafat_iniciarTransacao__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    uvafat_Instancia_ClienteXmlRpccImpl_* S_ = uvafat_Instancia_obterImpl_(S);
    uvafat_IdTransacao idTransacao = uvafat_IDNULO;

    xmlrpc_value* resultadoValue = NULL;
    xmlrpc_value* argumentosValue = NULL;
    xmlrpc_value* paramsStructValue = NULL;

    xmlrpc_value* idMeioPagamentoParamValue = NULL;
    xmlrpc_value* valorParamValue = NULL;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* idTransacaoValue = NULL;

    // Criando valores intermediários da chamada
    // xmlrpc_env_clean(&S_->env);
    argumentosValue = xmlrpc_array_new(&S_->env);
    if (!S_->env.fault_occurred) paramsStructValue = xmlrpc_struct_new(&S_->env);
    if (!S_->env.fault_occurred) idMeioPagamentoParamValue = xmlrpc_int_new(&S_->env, dados->idMeioPagamento);
    if (!S_->env.fault_occurred) valorParamValue = xmlrpc_double_new(&S_->env, dados->valor);

    // Construindo parâmetros da chamada
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "idMeioPagamento", idMeioPagamentoParamValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "valor", valorParamValue);
    
    if (!S_->env.fault_occurred) xmlrpc_array_append_item(&S_->env, argumentosValue, paramsStructValue);

    // Realização da chamada
    uvafat_Bool sucesso = 0;
    if (!S_->env.fault_occurred) xmlrpc_client_call2(&S_->env, S_->client, S_->serverInfo, "uvafat.iniciarTransacao", argumentosValue, &resultadoValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "sucesso", &sucessoValue);
    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, sucessoValue, (xmlrpc_bool*) &sucesso);

    if (!S_->env.fault_occurred) {
        if (!sucesso) {
            OBTER_ERRO_CHAMADA;

        } else {
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "idTransacao", &idTransacaoValue);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idTransacaoValue, &idTransacao);
        }
    }

    VERIFICAR_MOSTRAR_ERRO_XMLRPC;

    if (resultadoValue) xmlrpc_DECREF(resultadoValue);
    if (argumentosValue) xmlrpc_DECREF(argumentosValue);
    if (paramsStructValue) xmlrpc_DECREF(paramsStructValue);
    
    if (idMeioPagamentoParamValue) xmlrpc_DECREF(idMeioPagamentoParamValue);
    if (valorParamValue) xmlrpc_DECREF(valorParamValue);
    
    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (idTransacaoValue) xmlrpc_DECREF(idTransacaoValue);

    return idTransacao;
}

size_t uvafat_listarTransacoes__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosTransacao* dados, size_t qtdDados) {
    uvafat_Instancia_ClienteXmlRpccImpl_* S_ = uvafat_Instancia_obterImpl_(S);
    size_t qtdEncontrada = 0;
    
    if (dados == NULL) {
        uvafat_DadosUsuario dadosUsuario = { .id = idUsuario, .qtdTransacoes = 0 };
        uvafat_obterDadosUsuario__clienteXmlRpccImpl_(S, &dadosUsuario);
        return dadosUsuario.qtdTransacoes;
    }
    
    xmlrpc_value* idUsuarioPesquisaValue = NULL;
    xmlrpc_value* resultadoValue = NULL;
    xmlrpc_value* argumentosValue = NULL;
    xmlrpc_value* paramsStructValue = NULL;

    xmlrpc_value* sucessoValue = NULL;
    xmlrpc_value* qtdTransacoesValue = NULL;
    xmlrpc_value* transacoesValue = NULL;

    // Criando valores intermediários da chamada
    // xmlrpc_env_clean(&S_->env);
    argumentosValue = xmlrpc_array_new(&S_->env);
    if (!S_->env.fault_occurred) paramsStructValue = xmlrpc_struct_new(&S_->env);
    if (!S_->env.fault_occurred) idUsuarioPesquisaValue = xmlrpc_int_new(&S_->env, idUsuario);

    // Construindo parâmetros da chamada
    if (!S_->env.fault_occurred) xmlrpc_struct_set_value(&S_->env, paramsStructValue, "idUsuario", idUsuarioPesquisaValue);

    if (!S_->env.fault_occurred) xmlrpc_array_append_item(&S_->env, argumentosValue, paramsStructValue);

    // Realização da chamada
    uvafat_Bool sucesso = 0;
    if (!S_->env.fault_occurred) xmlrpc_client_call2(&S_->env, S_->client, S_->serverInfo, "uvafat.listarTransacoes", argumentosValue, &resultadoValue);
    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "sucesso", &sucessoValue);
    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, sucessoValue, (xmlrpc_bool*) &sucesso);

    if (!S_->env.fault_occurred) {
        if (!sucesso) {
            OBTER_ERRO_CHAMADA;

        } else {
            size_t qtdTransacoes = 0;
            size_t transacoesValue_size = 0;
            
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "qtdTransacoes", &qtdTransacoesValue);
            if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, resultadoValue, "transacoes", &transacoesValue);
            if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, qtdTransacoesValue, (int*) &qtdTransacoes);
            if (!S_->env.fault_occurred) transacoesValue_size = (size_t) xmlrpc_array_size(&S_->env, transacoesValue);
            
            if (qtdTransacoes != transacoesValue_size) LOG(S_, "[WARN] Tamanho da lista de transações (%d) não coincide com o tamanho informado no objeto de resposta (%d).\n", transacoesValue_size, qtdTransacoes);
            
            if (!S_->env.fault_occurred) {
                size_t i;
                for (i = 0; i < transacoesValue_size && i < qtdDados && !S_->env.fault_occurred; ++i) {
                    xmlrpc_value* transacaoValue = NULL;
                    xmlrpc_value* idTransacaoValue = NULL;
                    xmlrpc_value* idMeioPagamentoValue = NULL;
                    xmlrpc_value* idUsuarioValue = NULL;
                    xmlrpc_value* idEstadoValue = NULL;
                    xmlrpc_value* valorValue = NULL;
                    xmlrpc_value* terminadoValue = NULL;
                    xmlrpc_value* dataInicioValue = NULL;
                    xmlrpc_value* dataTerminoValue = NULL;
                    
                    uvafat_IdUsuario idUsuario_ = uvafat_IDNULO;
                    uvafat_IdTransacao idTransacao = uvafat_IDNULO;
                    uvafat_IdMeioPagamento idMeioPagamento = uvafat_IDNULO;
                    uvafat_IdEstadoTransacao idEstado = (uvafat_IdEstadoTransacao) 0;
                    uvafat_Bool terminado = 0;
                    double valor = 0;
                    xmlrpc_datetime dataInicio = {};
                    xmlrpc_datetime dataTermino = {};


                    if (!S_->env.fault_occurred) xmlrpc_array_read_item(&S_->env, transacoesValue, (unsigned int) i, &transacaoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "terminado", &terminadoValue);
                    if (!S_->env.fault_occurred) xmlrpc_read_bool(&S_->env, terminadoValue, &terminado);

                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "idTransacao", &idTransacaoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "idUsuario", &idUsuarioValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "idMeioPagamento", &idMeioPagamentoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "idEstado", &idEstadoValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "valor", &valorValue);
                    if (!S_->env.fault_occurred) xmlrpc_struct_read_value(&S_->env, transacaoValue, "dataInicio", &dataInicioValue);
                    if (!S_->env.fault_occurred && terminado) xmlrpc_struct_read_value(&S_->env, transacaoValue, "dataTermino", &dataTerminoValue);
                    
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idTransacaoValue, &idTransacao);
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idUsuarioValue, &idUsuario_);
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idMeioPagamentoValue, &idMeioPagamento);
                    if (!S_->env.fault_occurred) xmlrpc_read_int(&S_->env, idEstadoValue, (int*) &idEstado);
                    if (!S_->env.fault_occurred) xmlrpc_read_double(&S_->env, valorValue, &valor);
                    if (!S_->env.fault_occurred) xmlrpc_read_datetime(&S_->env, dataInicioValue, &dataInicio);
                    if (!S_->env.fault_occurred && dataTerminoValue) xmlrpc_read_datetime(&S_->env, dataTerminoValue, &dataTermino);

                    if (!S_->env.fault_occurred) {
                        struct tm dataInicioTm = {
                            .tm_year = dataInicio.Y - 1900,
                            .tm_mon = dataInicio.M - 1,
                            .tm_mday = dataInicio.D,
                            .tm_hour = dataInicio.h,
                            .tm_min = dataInicio.m,
                            .tm_sec = dataInicio.s
                        };
                        uvafat_DadosTransacao* transacao = &dados[i];
                        transacao->id = idTransacao;
                        transacao->idUsuario = idUsuario_;
                        transacao->idMeioPagamento = idMeioPagamento;
                        transacao->estado = idEstado;
                        transacao->valor = valor;
                        transacao->dataInicio = dataInicioTm;
                        if (dataTerminoValue) {
                            struct tm dataTerminoTm = {
                                .tm_year = dataInicio.Y - 1900,
                                .tm_mon = dataInicio.M - 1,
                                .tm_mday = dataInicio.D,
                                .tm_hour = dataInicio.h,
                                .tm_min = dataInicio.m,
                                .tm_sec = dataInicio.s
                            };
                            transacao->dataTermino = dataTerminoTm;
                        }
                        ++qtdEncontrada;
                    }
                    
                    if (transacaoValue) xmlrpc_DECREF(transacaoValue);
                    if (idTransacaoValue) xmlrpc_DECREF(idTransacaoValue);
                    if (idMeioPagamentoValue) xmlrpc_DECREF(idMeioPagamentoValue);
                    if (idUsuarioValue) xmlrpc_DECREF(idUsuarioValue);
                    if (idEstadoValue) xmlrpc_DECREF(idEstadoValue);
                    if (valorValue) xmlrpc_DECREF(valorValue);
                    if (dataInicioValue) xmlrpc_DECREF(dataInicioValue);
                    if (dataTerminoValue) xmlrpc_DECREF(dataTerminoValue);
                }
            }
        }
    }

    VERIFICAR_MOSTRAR_ERRO_XMLRPC;

    if (idUsuarioPesquisaValue) xmlrpc_DECREF(idUsuarioPesquisaValue);
    if (resultadoValue) xmlrpc_DECREF(resultadoValue);
    if (argumentosValue) xmlrpc_DECREF(argumentosValue);
    if (paramsStructValue) xmlrpc_DECREF(paramsStructValue);
    
    if (sucessoValue) xmlrpc_DECREF(sucessoValue);
    if (qtdTransacoesValue) xmlrpc_DECREF(qtdTransacoesValue);
    if (transacoesValue) xmlrpc_DECREF(transacoesValue);
    
    return qtdEncontrada;
}

void uvafat_finalizarTransacao__clienteXmlRpccImpl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    uvafat_Instancia_atribuirErro_(S, uvafat_ERR_NAOSUPORTADO, "Operação não suportada.");
}


/*
 * [[ IMPLEMENTAÇÃO: API INTERNA ]]]
 */


uvafat_Instancia_ClienteXmlRpccImpl_* uvafat_Instancia_obterImpl_(uvafat_Instancia_p S) {
    return (uvafat_Instancia_ClienteXmlRpccImpl_*) S->impl_p;
}

void LOG(uvafat_Instancia_ClienteXmlRpccImpl_* S_, char const* fmt, ...) {
    if (S_->arquivoLog) {
        va_list args;
        va_start(args, fmt);
        vfprintf(S_->arquivoLog, fmt, args);
        va_end(args);
    }
}

void ERR(uvafat_Instancia_ClienteXmlRpccImpl_* S_, char const* fmt, ...) {
    if (S_->arquivoErr) {
        va_list args;
        va_start(args, fmt);
        vfprintf(S_->arquivoErr, fmt, args);
        va_end(args);
    }
}

static uvafat_Bool uvafat_Instancia_criarClienteXmlRpcc_(
    uvafat_Instancia_p S,
    uvafat_Instancia_ClienteXmlRpccImpl_* S_,
    uvafat_DadosInstanciaCliente* dados,
    uvafat_IdErro* erro
) {
    xmlrpc_client_setup_global_const(&S_->env);

    if (S_->env.fault_occurred) {
        ERR(S_, "*ERRO ao configurar globais do XMLRPC-C! Código: %d, Mensagem: %s\n", S_->env.fault_code, S_->env.fault_string);
        goto error;
    }

    xmlrpc_client_create(
            &S_->env,
            XMLRPC_CLIENT_NO_FLAGS,
            "uvafat",
            "0.1.0",
            NULL,
            0,
            &S_->client);

    if (S_->env.fault_occurred) {
        ERR(S_, "*ERRO ao criar cliente XMLRPC-C! Código: %d, Mensagem: %s\n", S_->env.fault_code, S_->env.fault_string);
        goto error;
    }

    S_->serverInfo = xmlrpc_server_info_new(&S_->env, S_->url);
    if (S_->env.fault_occurred) {
            ERR(S_, "*ERRO ao criar informações do servidor XMLRPC-C! Código: %d, Mensagem: %s\n", S_->env.fault_code, S_->env.fault_string);
            goto error;
    }

    return 1;

error:
    return 0;
}

static void uvafat_Instancia_finalizarClienteXmlRpcc_(uvafat_Instancia_ClienteXmlRpccImpl_* S_) {
    xmlrpc_client_destroy(S_->client);
    xmlrpc_client_teardown_global_const();
}
