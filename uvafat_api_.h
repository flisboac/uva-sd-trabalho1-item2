// $flisboac 2017-10-22
/** 
 * @file uvafat_.h
 */
#ifndef UVA_SD_TRABALHO1_ITEM2_UVAFAT__H__
#define UVA_SD_TRABALHO1_ITEM2_UVAFAT__H__

#include <stdarg.h>
#include <string.h>
#include "uvafat.h"

#define uvafat_Instancia_porta_STD 8080

#define uvafat_Instancia_mensagemErro_TAMANHOMAXIMO 256

typedef struct uvafat_Instancia_Api_ uvafat_Instancia_Api_;

struct uvafat_Instancia {
    uvafat_Instancia_Api_ const* impl_c;
    void* impl_p;
    uvafat_IdErro idErro;
    char mensagemErro[uvafat_Instancia_mensagemErro_TAMANHOMAXIMO];
};

struct uvafat_Instancia_Api_ {

    void (*finalizar_f)(uvafat_Instancia_p S);

    uvafat_IdUsuario (*criarUsuario_f)(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);
    void (*obterDadosUsuario_f)(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);

    uvafat_IdMeioPagamento (*adicionarMeioPagamento_f)(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados);
    size_t (*listarMeiosPagamento_f)(uvafat_Instancia_p S, uvafat_IdUsuario usuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados);

    uvafat_IdTransacao (*iniciarTransacao_f)(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);
    size_t (*listarTransacoes_f)(uvafat_Instancia_p S, uvafat_IdUsuario usuario, uvafat_DadosTransacao* dados, size_t qtdDados);
    void (*finalizarTransacao_f)(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);
};


/*
 * [[[ FUNÇÕES INTERNAS ]]
 */


static inline uvafat_Bool uvafat_Instancia_ok_(uvafat_Instancia_p S) {
    return S->idErro == uvafat_ERR_OK;
}

static inline void uvafat_Instancia_limparErro_(uvafat_Instancia_p S) {
    S->idErro = uvafat_ERR_OK;
    S->mensagemErro[0] = '\0';
}

static inline void uvafat_Instancia_atribuirErro_(uvafat_Instancia_p S, uvafat_IdErro id, char const* mensagem) {
    S->idErro = id;
    S->mensagemErro[0] = '\0';
    strncat(S->mensagemErro, mensagem, uvafat_Instancia_mensagemErro_TAMANHOMAXIMO);
}

static inline void uvafat_Instancia_atribuirMensagemErro_(uvafat_Instancia_p S, uvafat_IdErro id, char const* formato, ...) {
    va_list args;
    va_start(args, formato);
    S->idErro = id;
    S->mensagemErro[0] = '\0';
    vsnprintf(S->mensagemErro, uvafat_Instancia_mensagemErro_TAMANHOMAXIMO, formato, args);
    va_end(args);
}

static inline void uvafat_Instancia_copiarErro_(uvafat_Instancia_p A, uvafat_Instancia_p B) {
    uvafat_Instancia_atribuirErro_(A, B->idErro, B->mensagemErro);
}

#endif //UVA_SD_TRABALHO1_ITEM2_UVAFAT__H__
