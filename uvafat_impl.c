// $flisboac 2017-10-22

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#include "uvafat.h"
#include "uvafat_api_.h"


/*
 * [[[ DEFINIÇÕES ]]]
 */


#define uvafat_Instancia_usuarios_MAX 16
#define uvafat_Instancia_meiosPagamento_MAX 32
#define uvafat_Instancia_transacoes_MAX 128


typedef struct uvafat_Instancia_Impl_ uvafat_Instancia_Impl_;
typedef struct uvafat_Usuario_Impl_ uvafat_Usuario_Impl_;
typedef struct uvafat_MeioPagamento_Impl_ uvafat_MeioPagamento_Impl_;
typedef struct uvafat_Transacao_Impl_ uvafat_Transacao_Impl_;


struct uvafat_MeioPagamento_Impl_ {
    uvafat_IdUsuario idUsuario;
    unsigned int anoVencimento;
    unsigned int mesVencimento;
    char numero[uvafat_MeioPagamento_numero_MAX];
    char cvv[uvafat_MeioPagamento_cvv_MAX];
};

struct uvafat_Transacao_Impl_ {
    uvafat_IdUsuario idUsuario;
    uvafat_IdMeioPagamento idMeioPagamento;
    uvafat_IdEstadoTransacao estado;
    double valor;
    time_t dataInicio;
    time_t dataTermino;
};

struct uvafat_Usuario_Impl_ {
    char nome[uvafat_Usuario_nome_MAX];
    size_t qtdTransacoes;
    size_t qtdMeiosPagamento;
};

struct uvafat_Instancia_Impl_ {
    size_t qtdUsuarios;
    size_t qtdMeiosPagamento;
    size_t qtdTransacoes;
    // P.S.: Sabemos que somos suspeitos por fazê-lo, mas prefira não usar arrays deste jeito!!
    // (por mais que a linguagem te "obrigue" a vc usar)
    uvafat_Usuario_Impl_ usuarios[uvafat_Instancia_usuarios_MAX];
    uvafat_MeioPagamento_Impl_ meiosPagamento[uvafat_Instancia_meiosPagamento_MAX];
    uvafat_Transacao_Impl_ transacoes[uvafat_Instancia_transacoes_MAX];
};

/*
 * [[ FUNÇÕES INTERNAS ]]]
 */


static void uvafat_finalizar__impl_(uvafat_Instancia_p S);

static uvafat_IdUsuario uvafat_criarUsuario__impl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);
static void uvafat_obterDadosUsuario__impl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados);

static uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento__impl_(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados);
static size_t uvafat_listarMeiosPagamento__impl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados);

static uvafat_IdTransacao uvafat_iniciarTransacao__impl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);
static size_t uvafat_listarTransacoes__impl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosTransacao* dados, size_t qtdDados);
static void uvafat_finalizarTransacao__impl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados);

static const uvafat_Instancia_Api_ uvafat_instancia__localImpl_ = {
    uvafat_finalizar__impl_,

    uvafat_criarUsuario__impl_,
    uvafat_obterDadosUsuario__impl_,

    uvafat_adicionarMeioPagamento__impl_,
    uvafat_listarMeiosPagamento__impl_,

    uvafat_iniciarTransacao__impl_,
    uvafat_listarTransacoes__impl_,
    uvafat_finalizarTransacao__impl_
};


/*
 * [[[ API PÚBLICA : IMPLEMENTAÇÃO ]]]
 */


uvafat_Instancia_p uvafat_criar(uvafat_IdErro* erro) {
    uvafat_IdErro resultado = uvafat_ERR_OK;
    uvafat_Instancia_p S = malloc(sizeof(struct uvafat_Instancia) + sizeof(uvafat_Instancia_Impl_));
    if (S) {
        uvafat_Instancia_Impl_ *S_ = (void*) S++;
        S_->qtdMeiosPagamento = 0;
        S_->qtdUsuarios = 0;
        S_->qtdTransacoes = 0;

        S->impl_c = &uvafat_instancia__localImpl_;
        S->impl_p = S_;
        uvafat_Instancia_limparErro_(S);

    } else {
        resultado = uvafat_ERR_MEM;
    }
    if (erro) *erro = resultado;
    return S;
}


/*
 * [[[ FUNÇÕES INTERNAS : IMPLEMENTAÇÃO ]]]
 */


static inline uvafat_Bool uvafat_MeioPagamento_isNumeroValido_(char const* numero, size_t qtdExataCaracteres) {
    uvafat_Bool todosCaracteresNumericos = 1;
    size_t tamanhoNumero = 0;

    if (numero != NULL) {
        while (*numero != '\0' && todosCaracteresNumericos && tamanhoNumero <= qtdExataCaracteres) {
            todosCaracteresNumericos = isdigit(*numero) != 0;
            tamanhoNumero++;
            numero++;
        }
    }

    return todosCaracteresNumericos && tamanhoNumero == qtdExataCaracteres;
}

static inline uvafat_Bool uvafat_MeioPagamento_isNumeroCartaoValido_(char const* numero) {
    return uvafat_MeioPagamento_isNumeroValido_(numero, uvafat_MeioPagamento_numero_MAX - 1);
}

static inline uvafat_Bool uvafat_MeioPagamento_isCvvCartaoValido_(char const* numero) {
    return uvafat_MeioPagamento_isNumeroValido_(numero, uvafat_MeioPagamento_cvv_MAX - 1);
}

static inline uvafat_Instancia_Impl_* uvafat_Instancia_obterImpl_(uvafat_Instancia_p S) {
    return (uvafat_Instancia_Impl_*) S->impl_p;
}

static inline uvafat_Usuario_Impl_* uvafat_Instancia_obterUsuario_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario) {
    uvafat_Usuario_Impl_* usuario = NULL;
    uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

    if (idUsuario == uvafat_IDNULO) {
        uvafat_Instancia_atribuirErro_(S, uvafat_ERR_IDNULO, "O ID do usuário não pode ser nulo.");

    } else {
        size_t indiceUsuario = (size_t) idUsuario - 1;

        if (indiceUsuario < 0 || indiceUsuario >= S_->qtdUsuarios) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_IDINVALIDO, "ID do usuário inválido.");

        } else {
            usuario = &S_->usuarios[indiceUsuario];
        }
    }

    return usuario;
}

static inline uvafat_Usuario_Impl_* uvafat_Instancia_obterUsuarioPorNome_(uvafat_Instancia_p S, char const* nome, uvafat_IdUsuario* retIdUsuario) {
    uvafat_Usuario_Impl_* usuario = NULL;
    uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

    if (!nome) {
        uvafat_Instancia_atribuirErro_(S, uvafat_ERR_NOMENULO, "O nome do usuário não pode ser nulo para esta pesquisa.");

    } else {
        size_t i;
        for (i = 0; i < S_->qtdUsuarios && usuario == NULL; ++i) {
            uvafat_Usuario_Impl_* usuarioIter = &S_->usuarios[i];
            if (strcmp(nome, usuarioIter->nome) == 0) {
                usuario = usuarioIter;
                if (retIdUsuario) *retIdUsuario = (uvafat_IdUsuario) i + 1;
            }
        }
    }

    return usuario;
}

static inline uvafat_MeioPagamento_Impl_* uvafat_Instancia_obterMeioPagamento_(uvafat_Instancia_p S, uvafat_IdMeioPagamento idMeioPagamento) {
    uvafat_MeioPagamento_Impl_* meioPagamento = NULL;
    uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

    if (idMeioPagamento == uvafat_IDNULO) {
        uvafat_Instancia_atribuirErro_(S, uvafat_ERR_IDNULO, "O ID do meio de pagamento não pode ser nulo.");

    } else {
        size_t indiceMeioPagamento = (size_t) idMeioPagamento - 1;

        if (indiceMeioPagamento < 0 || indiceMeioPagamento >= S_->qtdMeiosPagamento) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_IDINVALIDO, "ID do meio de pagamento inválido.");

        } else {
            meioPagamento = &S_->meiosPagamento[indiceMeioPagamento];
        }
    }

    return meioPagamento;
}

static void uvafat_finalizar__impl_(uvafat_Instancia_p S) {
    // Funciona apenas pq uma única alocação é feita. Veja a implementação de uvafat_criar
    if (S) free(S);
}

static uvafat_IdUsuario uvafat_criarUsuario__impl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    uvafat_Instancia_limparErro_(S);
    uvafat_IdUsuario id = uvafat_IDNULO;
    uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

    if (S_->qtdUsuarios < uvafat_Instancia_usuarios_MAX) {
        uvafat_Usuario_Impl_* usuario = &S_->usuarios[S_->qtdUsuarios++];
        dados->id = id = (uvafat_IdUsuario) S_->qtdUsuarios;
        dados->qtdTransacoes = usuario->qtdTransacoes = 0;
        dados->qtdMeiosPagamento = usuario->qtdMeiosPagamento = 0;
        strncat(usuario->nome, dados->nome, uvafat_Usuario_nome_MAX - 1);

    } else {
        uvafat_Instancia_atribuirErro_(S, uvafat_ERR_MEM, "Limite máximo de usuários atingido.");
    }

    return id;
}

static void uvafat_obterDadosUsuario__impl_(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    uvafat_Instancia_limparErro_(S);
    uvafat_IdUsuario idUsuario = dados->id;
    char const* nome = dados->nome;

    uvafat_Usuario_Impl_ *usuario = idUsuario != uvafat_IDNULO
        ? uvafat_Instancia_obterUsuario_(S, idUsuario)
        : nome != NULL ? uvafat_Instancia_obterUsuarioPorNome_(S, nome, &idUsuario) : NULL;

    if (usuario) {
        size_t i;
        dados->id = idUsuario;
        dados->nome_[0] = '\0'; strncat(dados->nome_, usuario->nome, uvafat_Usuario_nome_MAX); dados->nome = dados->nome_;
        dados->qtdMeiosPagamento = usuario->qtdMeiosPagamento;
        dados->qtdTransacoes = usuario->qtdTransacoes;
    }
}

static uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento__impl_(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados) {
    uvafat_Instancia_limparErro_(S);
    uvafat_IdMeioPagamento id = uvafat_IDNULO;
    uvafat_IdUsuario idUsuario = dados->idUsuario;
    uvafat_Usuario_Impl_ *usuario = uvafat_Instancia_obterUsuario_(S, idUsuario);

    if (usuario) {
        uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);
        time_t dataHoje;
        dataHoje = time(NULL);
        struct tm *infoHoje = gmtime(&dataHoje);

        if (S_->qtdMeiosPagamento >= uvafat_Instancia_meiosPagamento_MAX) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_MEM, "Limite máximo de meios de pagamento atingido.");

        } else if (!uvafat_MeioPagamento_isNumeroCartaoValido_(dados->numero)) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_CARTAOINVALIDO, "Número do cartão de crédito inválido.");

        } else if (!uvafat_MeioPagamento_isCvvCartaoValido_(dados->cvv)) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_CVVINVALIDO, "CVV do cartão de crédito inválido.");

        } else if (dados->mesVencimento < 1 || dados->mesVencimento > 12) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_VENCIMENTOINVALIDO, "Mês de vencimento inválido.");

        } else if (dados->anoVencimento < 1900 || dados->anoVencimento > 9999) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_VENCIMENTOINVALIDO, "Ano de vencimento inválido.");

        } else if ((1900 + infoHoje->tm_year) > dados->anoVencimento || (1 + infoHoje->tm_mon) > dados->mesVencimento) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_CARTAOVENCIDO, "Cartão de crédito já vencido.");

        } else {
            uvafat_MeioPagamento_Impl_ *meioPagamento = &S_->meiosPagamento[S_->qtdMeiosPagamento++];
            meioPagamento->idUsuario = dados->id = id = (uvafat_IdMeioPagamento) S_->qtdMeiosPagamento;
            meioPagamento->mesVencimento = dados->mesVencimento;
            meioPagamento->anoVencimento = dados->anoVencimento;
            meioPagamento->numero[0] = '\0'; strncat(meioPagamento->numero, dados->numero, uvafat_MeioPagamento_numero_MAX - 1);
            meioPagamento->cvv[0] = '\0'; strncat(meioPagamento->cvv, dados->cvv, uvafat_MeioPagamento_cvv_MAX - 1);

            usuario->qtdMeiosPagamento++;
        }
    }

    return id;
}

static size_t uvafat_listarMeiosPagamento__impl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados) {
    uvafat_Instancia_limparErro_(S);
    size_t qtdProcessada = 0;
    uvafat_Usuario_Impl_ *usuario = uvafat_Instancia_obterUsuario_(S, idUsuario);

    if (usuario) {
        uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

        if (!dados) {
            qtdProcessada = usuario->qtdMeiosPagamento;

        } else {
            size_t i, j;

            for (i = 0; i < S_->qtdMeiosPagamento && qtdProcessada < qtdDados; ++i) {
                uvafat_MeioPagamento_Impl_* meioPagamento = &S_->meiosPagamento[i];

                if (meioPagamento->idUsuario == idUsuario) {
                    dados->id = (uvafat_IdMeioPagamento) i + 1;
                    dados->idUsuario = idUsuario;
                    dados->numero_[0] = '\0';
                    strncat(dados->numero_, meioPagamento->numero, uvafat_MeioPagamento_numero_MAX - 1);
                        dados->numero = dados->numero_;
                        for (j = 4; j < 12; ++j) dados->numero_[j] = 'X';
                    dados->cvv_[0] = '\0'; strncat(dados->cvv_, meioPagamento->cvv, uvafat_MeioPagamento_cvv_MAX - 1); dados->cvv = dados->cvv_;
                        for (j = 0; dados->cvv_[j] != '\0' && j < uvafat_MeioPagamento_cvv_MAX; ++j) dados->cvv_[j] = 'X';
                    dados->anoVencimento = meioPagamento->anoVencimento;
                    dados->mesVencimento = meioPagamento->mesVencimento;

                    qtdProcessada++;
                    dados++;
                }
            }
        }
    }

    return qtdProcessada;
}

static uvafat_IdTransacao uvafat_iniciarTransacao__impl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    uvafat_Instancia_limparErro_(S);
    uvafat_IdTransacao idTransacao = uvafat_IDNULO;
    uvafat_IdMeioPagamento idMeioPagamento = dados->idMeioPagamento;
    uvafat_MeioPagamento_Impl_ *meioPagamento = uvafat_Instancia_obterMeioPagamento_(S, idMeioPagamento);
    uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

    if (meioPagamento) {
        if (dados->valor <= 0) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_VALORINVALIDO, "Valor da transação inválido.");

        } else if (S_->qtdTransacoes >= uvafat_Instancia_transacoes_MAX) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_MEM, "Máximo de transações atingido.");

        } else {
            uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);
            uvafat_Transacao_Impl_ *transacao = &S_->transacoes[S_->qtdTransacoes++];
            dados->id = idTransacao = (uvafat_IdTransacao) S_->qtdTransacoes;
            transacao->idUsuario = dados->idUsuario = meioPagamento->idUsuario;
            transacao->idMeioPagamento = idMeioPagamento;
            transacao->estado = dados->estado = uvafat_Transacao_INICIADA;
            transacao->valor = dados->valor;
            transacao->dataInicio = time(NULL); dados->dataInicio = *gmtime(&transacao->dataInicio);
            transacao->dataTermino = (time_t)-1;

            uvafat_Usuario_Impl_ *usuario = uvafat_Instancia_obterUsuario_(S, meioPagamento->idUsuario);
            assert(usuario);
            usuario->qtdTransacoes++;
        }
    }

    return idTransacao;
}

static size_t uvafat_listarTransacoes__impl_(uvafat_Instancia_p S, uvafat_IdUsuario idUsuario, uvafat_DadosTransacao* dados, size_t qtdDados) {
    uvafat_Instancia_limparErro_(S);
    size_t qtdProcessada = 0;
    uvafat_Usuario_Impl_ *usuario = uvafat_Instancia_obterUsuario_(S, idUsuario);

    if (usuario) {
        uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);

        if (!dados) {
            qtdProcessada = usuario->qtdTransacoes;

        } else {
            size_t i;

            for (i = 0; i < S_->qtdTransacoes && qtdProcessada < qtdDados; ++i) {
                uvafat_Transacao_Impl_* transacao = &S_->transacoes[i];

                if (transacao->idUsuario == idUsuario) {
                    dados->id = (uvafat_IdTransacao) i + 1;
                    dados->idUsuario = idUsuario;
                    dados->idMeioPagamento = transacao->idMeioPagamento;
                    dados->estado = transacao->estado;
                    dados->valor = transacao->valor;
                    dados->dataInicio = *gmtime(&transacao->dataInicio);
                    dados->dataTermino = *gmtime(&transacao->dataTermino);

                    qtdProcessada++;
                    dados++;
                }
            }
        }
    }

    return qtdProcessada;
}

static void uvafat_finalizarTransacao__impl_(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    uvafat_Instancia_Impl_* S_ = uvafat_Instancia_obterImpl_(S);
    uvafat_IdTransacao idTransacao = dados->id;

    if (idTransacao == uvafat_IDNULO) {
        uvafat_Instancia_atribuirErro_(S, uvafat_ERR_IDNULO, "O ID da transação não pode ser nulo.");

    } else {
        size_t indiceTransacao = (size_t) idTransacao - 1;

        if (indiceTransacao < 0 || indiceTransacao >= S_->qtdTransacoes) {
            uvafat_Instancia_atribuirErro_(S, uvafat_ERR_IDINVALIDO, "ID da transação é inválido.");

        } else {
            uvafat_Transacao_Impl_ *transacao = &S_->transacoes[indiceTransacao];
            uvafat_IdEstadoTransacao estadoAtual = transacao->estado;
            uvafat_IdEstadoTransacao novoEstado = dados->estado;

            if (estadoAtual == uvafat_Transacao_APROVADA || estadoAtual == uvafat_Transacao_REJEITADA) {
                uvafat_Instancia_atribuirErro_(S, uvafat_ERR_FINALIZADO, "Transação já está finalizada, operação não permitida.");

            } else if (novoEstado == estadoAtual || novoEstado == uvafat_Transacao_INICIADA) {
                uvafat_Instancia_atribuirErro_(S, uvafat_ERR_NOVOESTADOINVALIDO, "O novo estado solicitado para a transação não é válido.");

            } else {
                transacao->estado = novoEstado;
                transacao->dataTermino = time(NULL);

                dados->idUsuario = transacao->idUsuario;
                dados->idMeioPagamento = transacao->idMeioPagamento;
                dados->estado = transacao->estado;
                dados->valor = transacao->valor;
                dados->dataInicio = *gmtime(&transacao->dataInicio);
                dados->dataTermino = *gmtime(&transacao->dataTermino);
            }
        }
    }
}
