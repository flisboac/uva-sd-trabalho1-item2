// $flisboac 2017-10-22

#include <stdlib.h>
#include <string.h>

#include "uvafat.h"
#include "uvafat_api_.h"


/*
 * [[[ API PÚBLICA ]]]
 */


void uvafat_finalizar(uvafat_Instancia_p S) {
    if (S) S->impl_c->finalizar_f(S);
}

uvafat_Bool uvafat_ok(uvafat_Instancia_p S) {
    return uvafat_Instancia_ok_(S);
}

uvafat_IdErro uvafat_obterIdErro(uvafat_Instancia_p S) {
    return S->idErro;
}

char const* uvafat_obterMensagemErro(uvafat_Instancia_p S) {
    return S->mensagemErro;
}

uvafat_IdUsuario uvafat_criarUsuario(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    return S->impl_c->criarUsuario_f(S, dados);
}

void uvafat_obterDadosUsuario(uvafat_Instancia_p S, uvafat_DadosUsuario* dados) {
    return S->impl_c->obterDadosUsuario_f(S, dados);
}

uvafat_IdMeioPagamento uvafat_adicionarMeioPagamento(uvafat_Instancia_p S, uvafat_DadosMeioPagamento* dados) {
    return S->impl_c->adicionarMeioPagamento_f(S, dados);
}

size_t uvafat_listarMeiosPagamento(uvafat_Instancia_p S, uvafat_IdUsuario usuario, uvafat_DadosMeioPagamento* dados, size_t qtdDados) {
    return S->impl_c->listarMeiosPagamento_f(S, usuario, dados, qtdDados);
}

uvafat_IdTransacao uvafat_iniciarTransacao(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    return S->impl_c->iniciarTransacao_f(S, dados);
}

size_t uvafat_listarTransacoes(uvafat_Instancia_p S, uvafat_IdUsuario usuario, uvafat_DadosTransacao* dados, size_t qtdDados) {
    return S->impl_c->listarTransacoes_f(S, usuario, dados, qtdDados);
}

void uvafat_finalizarTransacao(uvafat_Instancia_p S, uvafat_DadosTransacao* dados) {
    S->impl_c->finalizarTransacao_f(S, dados);
}

char const* uvafat_obterSimboloEstadoTransacao(uvafat_IdEstadoTransacao estado) {
    switch (estado) {
    case uvafat_Transacao_INICIADA: return "INICIADA";
    case uvafat_Transacao_APROVADA: return "APROVADA";
    case uvafat_Transacao_REJEITADA: return "REJEITADA";
    default: return "???";
    }
}

uvafat_Bool uvafat_isEstadoTransacaoTerminal(uvafat_IdEstadoTransacao estado) {
    switch (estado) {
        case uvafat_Transacao_APROVADA:
        case uvafat_Transacao_REJEITADA:
            return 1;
        default:
            return 0;
    }
}
