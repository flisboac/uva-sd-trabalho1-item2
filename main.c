
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>

#include "uvafat.h"

#define URL_BASE "http://localhost"
#define URI_BASE "/RPC2"

typedef enum IdModoPrograma_ {
    IdModoPrograma_INVALIDO_,
    IdModoPrograma_LOCAL_,
    IdModoPrograma_CLIENTE_,
    IdModoPrograma_SERVIDOR_
} IdModoPrograma_;

typedef struct EstadoPrograma_ {

    IdModoPrograma_ modo;
    int retorno;
    uvafat_Instancia_p inst;

} EstadoPrograma_;


/*
 * [[[ FUNÇÕES (DECLARAÇÕES) / MAIN ]]]
 */


static void onCancelarPrograma(int sinal);

static void iniciarPrograma(int argc, char const** argv);
static void finalizarPrograma();

static void executarCliente();
static void executarServidor();

static EstadoPrograma_ E = {};

int main(int argc, char const** argv) {
    setlocale(LC_ALL, "");

    if (signal(SIGTERM, onCancelarPrograma) == SIG_ERR) {
        printf("*ERRO ao atribuir o handler para SIGTERM!\n");
        return EXIT_FAILURE;
    }

    if (signal(SIGINT, onCancelarPrograma) == SIG_ERR) {
        printf("*ERRO ao atribuir o handler para SIGINT!\n");
        return EXIT_FAILURE;
    }

    iniciarPrograma(argc, argv);

    switch(E.modo) {
    case IdModoPrograma_LOCAL_:
    case IdModoPrograma_CLIENTE_:
        executarCliente();
        break;

    case IdModoPrograma_SERVIDOR_:
        executarServidor();
        break;

    case IdModoPrograma_INVALIDO_:
        break;
    }

    finalizarPrograma();
	return E.retorno;
}


static void onCancelarPrograma(int sinal) {
    printf("*ATENCAO Cancelamento do programa solicitado com sinal %d!\n", sinal);
    finalizarPrograma();
    exit(EXIT_SUCCESS);
}

static void iniciarPrograma(int argc, char const** argv) {
    uvafat_Bool valido = 1;
    char const* strModo = argc > 1 ? argv[1] : "local";
    char const* strPorta = argc > 2 ? argv[2] : "8080";
    char const* chaveApi = argc > 3 ? argv[3] : NULL;

    E.modo = strcmp(strModo, "local") == 0
        ? IdModoPrograma_LOCAL_
        : strcmp(strModo, "cliente") == 0
        ? IdModoPrograma_CLIENTE_
        : strcmp(strModo, "servidor") == 0
        ? IdModoPrograma_SERVIDOR_
        : IdModoPrograma_INVALIDO_;

    char* strPortaC = NULL;
    long porta = (unsigned int) strtol(strPorta, &strPortaC, 10);
    if (strPorta == strPortaC || porta <= 0 || porta >= 65536) {
        printf("ERRO: Número de porta '%s' inválida!\n", strPorta);
        E.retorno = EXIT_FAILURE;
        return;
    }

    char urlConexao[256] = URL_BASE;
    strncat(urlConexao, ":", sizeof(urlConexao));
    strncat(urlConexao, strPorta, sizeof(urlConexao)); // TODO construir o strPorta novamente, para evitar problemas com espaços no início

    uvafat_IdErro idErro = uvafat_ERR_OK;
    switch(E.modo) {
    case IdModoPrograma_LOCAL_:
        E.inst = uvafat_criar(&idErro);
        if (idErro) {
            printf("ERRO ao criar instância de aplicação local! Código do erro: %d\n", idErro);
            return;
        }
        break;
    case IdModoPrograma_CLIENTE_: {
        strncat(urlConexao, URI_BASE, sizeof(urlConexao));
        uvafat_DadosInstanciaCliente dadosCliente = {
            //.chaveApi = chaveApi,
            .endpointUrl = urlConexao,
            .arquivoLog = stderr,
            .arquivoErro = stderr
        };
        E.inst = uvafat_conectar(&dadosCliente, &idErro);
        if (idErro) {
            printf("ERRO ao conectar cliente RPC à aplicação remota! Código do erro: %d\n", idErro);
            return;
        }
    }
        break;
    case IdModoPrograma_SERVIDOR_: {
        uvafat_DadosInstanciaServidor dadosServidor = {
            //.chaveApiGlobal = chaveApi,
            .contextoAplicacao = URI_BASE,
            .numeroPorta = (unsigned int) porta,
            .arquivoLog = stderr,
            .arquivoErro = stderr,
            .nomeArquivoLog = "uvafat.log"
        };
        E.inst = uvafat_iniciarServidor(&dadosServidor, &idErro);
        if (idErro) {
            printf("ERRO ao iniciar servidor RPC! Código do erro: %d\n", idErro);
            return;
        }
    }
        break;
    default:
        printf("ERRO: Modo de operação '%s' inválido!", strModo);
        E.retorno = EXIT_FAILURE;
        return;
    }
}

static void finalizarPrograma() {
    uvafat_finalizar(E.inst);
}

static char* lerString(char const* mensagem, char* buffer, size_t tamBuffer) {
    if (mensagem) printf(mensagem);
    char* ret = fgets(buffer, (int) tamBuffer, stdin);
    char* rev_buffer = buffer + tamBuffer - 1;
    while (*rev_buffer == '\0') rev_buffer--;
    if (*rev_buffer == '\n') {
        *rev_buffer = '\0';
    } else {
        while (fgetc(stdin) != '\n');
    }
    return ret;
}

static int lerInt(char const* mensagem, int* buffer) {
    if (mensagem) printf(mensagem);
    int ret = fscanf(stdin, "%d", buffer); while (fgetc(stdin) != '\n');
    return ret;
}

static int lerLong(char const* mensagem, long* buffer) {
    if (mensagem) printf(mensagem);
    int ret = fscanf(stdin, "%ld", buffer); while (fgetc(stdin) != '\n');
    return ret;
}

static int lerDouble(char const* mensagem, double* buffer) {
    if (mensagem) printf(mensagem);
    int ret = fscanf(stdin, "%lf", buffer); while (fgetc(stdin) != '\n');
    return ret;
}

static int lerId(char const* mensagem, uvafat_Id* buffer) {
    if (mensagem) printf(mensagem);
    int ret = fscanf(stdin, uvafat_IDP, buffer); while (fgetc(stdin) != '\n');
    return ret;
}

static uvafat_Bool lerDadosUsuario(uvafat_DadosUsuario* dadosUsuario) {

    char nomeUsuario[uvafat_Usuario_nome_MAX] = "";
    lerString("Digite o nome ou ID do usuário: ", nomeUsuario, uvafat_Usuario_nome_MAX);

    char const* ptrNomeUsuario = nomeUsuario;
    uvafat_IdUsuario idUsuario = uvafat_IDNULO;
    uvafat_Bool numero = 1;
    while (*ptrNomeUsuario && numero) {
        numero = isdigit(*ptrNomeUsuario++) != 0;
    };
    if (numero) idUsuario = (int) atol(nomeUsuario);
    dadosUsuario->id = idUsuario;
    dadosUsuario->nome = nomeUsuario;

    uvafat_obterDadosUsuario(E.inst, dadosUsuario);
    if (!uvafat_ok(E.inst)) {
        printf("*ERRO ao buscar dados do usuário. Código: %d, Mensagem: %s\n",
            uvafat_obterIdErro(E.inst),
            uvafat_obterMensagemErro(E.inst));
        return 0;

    } else if (dadosUsuario->id == uvafat_IDNULO) {
        printf("*Usuário não encontrado.\n");
        return 0;
    }

    return 1;
};

static void executarCliente() {
    uvafat_IdUsuario usuarioSelecionado = uvafat_IDNULO;
    enum {
        OPCAO_INVALIDA = -1,
        OPCAO_CRIAR_USUARIO = 1,
        OPCAO_BUSCAR_USUARIO,
        OPCAO_CADASTRAR_MEIO_PAGAMENTO,
        OPCAO_INICIAR_TRANSACAO,
        OPCAO_FINALIZAR_TRANSACAO,
        OPCAO_SAIR = 9
    };
    int opcao = OPCAO_INVALIDA;

    //fflush(stdin);
    while (opcao != OPCAO_SAIR) {
        printf("[[ SISTEMA DE BILLING ]]\n");
        printf("------------------------\n");
        printf("MENU PRINCIPAL\n");
        printf("(%d) Cadastrar novo usuário\n", OPCAO_CRIAR_USUARIO);
        printf("(%d) Buscar usuário\n", OPCAO_BUSCAR_USUARIO);
        printf("(%d) Cadastrar novo meio de pagamento\n", OPCAO_CADASTRAR_MEIO_PAGAMENTO);
        printf("(%d) Iniciar transação\n", OPCAO_INICIAR_TRANSACAO);
        if (E.modo == IdModoPrograma_LOCAL_) printf("(%d) Finalizar transação\n", OPCAO_FINALIZAR_TRANSACAO);
        printf("\n");
        printf("(%d) Sair do programa\n", OPCAO_SAIR);
        lerInt("Selecione uma opção: ", &opcao);

        switch(opcao) {
        case OPCAO_CRIAR_USUARIO: {
            char nomeUsuario[uvafat_Usuario_nome_MAX] = "";
            lerString("Digite o nome do novo usuário: ", nomeUsuario, uvafat_Usuario_nome_MAX);
            uvafat_DadosUsuario dadosUsuario = {
                .nome = nomeUsuario
            };
            uvafat_IdUsuario idUsuario = uvafat_criarUsuario(E.inst, &dadosUsuario);
            if (!uvafat_ok(E.inst)) {
                printf("*ERRO ao criar usuário. Código: %d, Mensagem: %s\n",
                    uvafat_obterIdErro(E.inst),
                    uvafat_obterMensagemErro(E.inst));
            } else {
                printf("* Usuário '%s' (ID:"uvafat_IDP") criado com sucesso.\n", nomeUsuario, idUsuario);
            }

        } break;
        case OPCAO_BUSCAR_USUARIO: {
            uvafat_DadosUsuario dadosUsuario;
            if (lerDadosUsuario(&dadosUsuario)) {
                printf("- %s [ID:"uvafat_IDP"]\n", dadosUsuario.nome, dadosUsuario.id);
                printf("  - Meios de Pagamento [Total:%zd]\n", dadosUsuario.qtdMeiosPagamento);
                if (dadosUsuario.qtdMeiosPagamento > 0) {
                    uvafat_DadosMeioPagamento* dadosMeiosPagamento = malloc(
                            sizeof(uvafat_DadosMeioPagamento) * dadosUsuario.qtdMeiosPagamento);

                    if (!dadosMeiosPagamento) {
                        printf("    *ERRO Falha ao alocar memória para buscar os meios de pagamento do usuário!\n");

                    } else {
                        size_t qtdDadosMeiosPagamento = uvafat_listarMeiosPagamento(
                                E.inst, dadosUsuario.id, dadosMeiosPagamento, dadosUsuario.qtdMeiosPagamento);

                        if (!uvafat_ok(E.inst)) {
                            printf("    *ERRO ao buscar meios de pagamento do usuário. Código: %d, Mensagem: %s\n",
                                uvafat_obterIdErro(E.inst),
                                uvafat_obterMensagemErro(E.inst));

                        } else {
                            size_t i;
                            for (i = 0; i < qtdDadosMeiosPagamento; ++i) {
                                uvafat_DadosMeioPagamento* mp = &dadosMeiosPagamento[i];
                                printf("    - [ID:"uvafat_IDP"] Cartão: %s, Data de vencimento: %02d/%04d\n",
                                    mp->id, mp->numero, mp->mesVencimento, mp->anoVencimento);
                            }
                        }

                        free(dadosMeiosPagamento);
                    }
                }
                printf("  - Transações [Total:%zd]\n", dadosUsuario.qtdTransacoes);
                if (dadosUsuario.qtdTransacoes > 0) {
                    uvafat_DadosTransacao* dadosTransacoes = malloc(
                            sizeof(uvafat_DadosTransacao) * dadosUsuario.qtdTransacoes);

                    if (!dadosTransacoes) {
                        printf("   *ERRO Falha ao alocar memória para buscar as transações do usuário!\n");

                    } else {
                        size_t qtdDadosTransacao = uvafat_listarTransacoes(
                                E.inst, dadosUsuario.id, dadosTransacoes, dadosUsuario.qtdTransacoes);

                        if (!uvafat_ok(E.inst)) {
                            printf("   *ERRO ao buscar dados das transações do usuário. Código: %d, Mensagem: %s\n",
                                uvafat_obterIdErro(E.inst),
                                uvafat_obterMensagemErro(E.inst));

                        } else {
                            size_t i;
                            for (i = 0; i < qtdDadosTransacao; ++i) {
                                uvafat_DadosTransacao* tr = &dadosTransacoes[i];
                                printf("    - [ID:"uvafat_IDP"] MeioPagamento:"uvafat_IDP, tr->id, tr->idMeioPagamento);
                                printf(", Estado: %s", uvafat_obterSimboloEstadoTransacao(tr->estado));
                                printf(", Valor: $%.03f", tr->valor);
                                printf(", Início: %s", asctime(&tr->dataInicio));
                                if (tr->estado == uvafat_Transacao_APROVADA || tr->estado == uvafat_Transacao_REJEITADA) {
                                    printf(", Término: %s", asctime(&tr->dataTermino));
                                }
                                printf("\n");
                            }
                        }

                        free(dadosTransacoes);
                    }
                }
            }

        } break;
        case OPCAO_CADASTRAR_MEIO_PAGAMENTO: {
            uvafat_DadosUsuario dadosUsuario;

            if (lerDadosUsuario(&dadosUsuario)) {
                char numeroCartao[uvafat_MeioPagamento_numero_MAX] = "";
                char cvvCartao[uvafat_MeioPagamento_cvv_MAX] = "";
                int mesVencimento = -1;
                int anoVencimento = -1;

                lerString("Digite o NÚMERO do cartão de crédito (com 16 dígitos e sem pontuações): ",
                        numeroCartao, uvafat_MeioPagamento_numero_MAX);

                lerString("Digite o CVV do cartão de crédito (com 3 dígitos e sem pontuações): ",
                        cvvCartao, uvafat_MeioPagamento_cvv_MAX);

                lerInt("Digite o MÊS do vencimento do cartão de crédito (com 2 dígitos): ", &mesVencimento);

                lerInt("Digite o ANO do vencimento do cartão de crédito (com 4 dígitos): ", &anoVencimento);

                uvafat_DadosMeioPagamento dadosMeioPagamento = {
                    .idUsuario = dadosUsuario.id,
                    .numero = numeroCartao,
                    .cvv = cvvCartao,
                    .anoVencimento = (unsigned int) anoVencimento,
                    .mesVencimento = (unsigned int) mesVencimento
                };

                uvafat_IdMeioPagamento idMeioPagamento = uvafat_adicionarMeioPagamento(E.inst, &dadosMeioPagamento);
                if (!uvafat_ok(E.inst)) {
                    printf("*ERRO ao cadastrar novo meio de pagamento. Código: %d, Mensagem: %s\n",
                        uvafat_obterIdErro(E.inst),
                        uvafat_obterMensagemErro(E.inst));

                } else {
                    printf("Meio de pagamento (ID:"uvafat_IDP") cadastrado com sucesso.\n", idMeioPagamento);
                }
            }
        } break;
        case OPCAO_INICIAR_TRANSACAO: {
            uvafat_DadosUsuario dadosUsuario;

            if (lerDadosUsuario(&dadosUsuario)) {

                if (dadosUsuario.qtdMeiosPagamento == 0) {
                    printf("Não há meios de pagamento disponíveis para este usuário.\n");

                } else {
                    uvafat_DadosMeioPagamento* dadosMeiosPagamento = malloc(
                        sizeof(uvafat_DadosMeioPagamento) * dadosUsuario.qtdMeiosPagamento);

                    if (!dadosMeiosPagamento) {
                        printf("*ERRO Falha ao alocar memória para buscar os meios de pagamento do usuário!\n");

                    } else {
                        size_t qtdDadosMeiosPagamento = uvafat_listarMeiosPagamento(
                                E.inst, dadosUsuario.id, dadosMeiosPagamento, dadosUsuario.qtdMeiosPagamento);

                        if (!uvafat_ok(E.inst)) {
                            printf("*ERRO ao buscar meios de pagamento do usuário. Código: %d, Mensagem: %s\n",
                                uvafat_obterIdErro(E.inst),
                                uvafat_obterMensagemErro(E.inst));

                        } else {
                            uvafat_IdMeioPagamento idMeioPagamento = -1;
                            double valor = 0;

                            while (idMeioPagamento < 0) {
                                size_t i;

                                printf("Há %zd meios de pagamento disponíveis:\n", qtdDadosMeiosPagamento);

                                for (i = 0; i < qtdDadosMeiosPagamento; ++i) {
                                    uvafat_DadosMeioPagamento* mp = &dadosMeiosPagamento[i];
                                    printf("(%zd) Meio de Pagamento ID:"uvafat_IDP", Cartão: %s, Data de vencimento: %02d/%04d\n",
                                        i + 1, mp->id, mp->numero, mp->mesVencimento, mp->anoVencimento);
                                }

                                lerLong("Selecione o meio de pagamento desejado: ", &i);

                                if (i <= 0 || i > qtdDadosMeiosPagamento) {
                                    printf("Opção inválida.\n");

                                } else {
                                    idMeioPagamento = dadosMeiosPagamento[i - 1].id;
                                }
                            }

                            lerDouble("Digite o valor total da transação: ", &valor);

                            uvafat_DadosTransacao dadosTransacao = {
                                    .idUsuario = dadosUsuario.id,
                                    .idMeioPagamento = idMeioPagamento,
                                    .valor = valor
                            };

                            uvafat_IdTransacao idTransacao = uvafat_iniciarTransacao(E.inst, &dadosTransacao);
                            if (!uvafat_ok(E.inst)) {
                                printf(
                                        "*ERRO ao iniciar transação. Código: %d, Mensagem: %s\n",
                                        uvafat_obterIdErro(E.inst),
                                        uvafat_obterMensagemErro(E.inst));

                            } else {
                                printf("Transação (ID:"uvafat_IDP") iniciada com sucesso.\n", idTransacao);
                            }
                        }

                        free(dadosMeiosPagamento);
                    }
                }
            }

        } break;
        case OPCAO_FINALIZAR_TRANSACAO: {
            if (E.modo == IdModoPrograma_LOCAL_) break;



        } break;
        case OPCAO_SAIR: {
            printf("Saindo...\n");

        } break;
        default:
            printf("Opção '%d' incorreta. Tente novamente.\n", opcao);
            break;
        }

        printf("\n");
    }
}

static void executarServidor() {
    printf("Ainda não implementado. Tente mais tarde!\n");
}
