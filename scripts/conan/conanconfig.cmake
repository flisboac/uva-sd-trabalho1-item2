
#set(CONAN_PROFILE_NAME ${CMAKE_SOURCE_DIR}/scripts/conan/conanconfig.gcc.x64.txt)

#find_program(CONAN conan)
#MESSAGE(STATUS "Found Conan in: ${CONAN}")
#execute_process(
#        COMMAND ${CONAN} install ${CMAKE_SOURCE_DIR} --profile ${CONAN_PROFILE_NAME} --build missing -s build_type=Debug -e CC=${CMAKE_C_COMPILER}
#        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
#)
#include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
#conan_basic_setup()

find_program(CONAN conan)
MESSAGE(STATUS "Found Conan in: ${CONAN}")
execute_process(
        COMMAND ${CONAN} install ${CMAKE_SOURCE_DIR} --build missing
        WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup(TARGET)