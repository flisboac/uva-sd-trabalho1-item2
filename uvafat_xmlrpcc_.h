// $flavio.lisboa @ 2017-10-27.
//
/*
 * @file uvafat_xmlrpcc.h
 */


#ifndef UVA_SD_TRABALHO1_ITEM2_UVAFAT_XMLRPCC_H
#define UVA_SD_TRABALHO1_ITEM2_UVAFAT_XMLRPCC_H

#define CURLOPT_HTTP_VERSION CURL_HTTP_VERSION_1_1
#include <time.h>

// XMLRPC-C stuff
#ifdef _WIN32
#  include <windows.h>
#  include <winsock2.h>
#else
#  include <unistd.h>
#endif

#include <xmlrpc-c/base.h>

#include "uvafat.h"
#include "uvafat_api_.h"

#endif // UVA_SD_TRABALHO1_ITEM2_UVAFAT_XMLRPCC_H
