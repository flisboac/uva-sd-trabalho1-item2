Para compilar, é necessário ter os seguintes acotes instalados no sistema operacional:

- gcc
- make
- cmake
- conan (e suas dependências, e.g. python3)
- nghttp2
- psl

Depois, executar os comandos (considerando o build feito em uma pasta chamada `build`):

```
$ mkdir build && cd build
$ conan install ..
$ cmake ..
$ make
```

Para executar o projeto, execute `uvafat` na pasta `build/bin`. É preferível que dois terminais sejam abertos para executar o programa. Por exemplo:

```
# Executando o servidor RPC (como processo em background)
$ build/bin/uvafat servidor &

# Executando o cliente RPC
$ build/bin/uvafat cliente
```

